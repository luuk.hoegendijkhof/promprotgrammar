""" This file contains the functions used to train the sequence-to-sequence models.
It contains the loss function used for the LSTM-LSTM, a training function, an the
functions used to generate the data for the thesis.
"""
from datetime import datetime
import random

from Bio import Align
from d2l import torch as d2l
import torch
from torch import nn

from Seq2Seq_LoadData import get_promoter_vocab, get_protein_vocab, untokenize_sequence, load_dataset
from Seq2Seq_Model import get_LSTM_LSTM_model
import collections
from scipy.stats import entropy as scipy_entropy


def sequence_mask(X, valid_len, value=0):   
    """Mask irrelevant entries in a pytorch tensor.
       In this project it is used to set the weights of padding tokens to zero.

    X: input tensor.
    valid_len: torch integer valid length of the sequence (without padding tokens)
    value: masking value (0 to not use)
    returns: masked tensor.
    """
    maxlen = X.size(1)
    mask = torch.arange((maxlen), dtype=torch.float32,
                        device=X.device)[None, :] < valid_len[:, None]
    X[~mask] = value
    return X


class MaskedSoftmaxCELoss(nn.CrossEntropyLoss):
    """The softmax cross-entropy loss with masks."""
    def forward(self, pred, label, valid_len):
        """
        pred: predicted tensor with shape: (`batch_size`, `num_steps`, `vocab_size`)
        label: true sequence tensor with shape: (`batch_size`, `num_steps`)
        valid_len: true protein sequence length tensor with shape: (`batch_size`,)
        """
        # assume all weights have a weight of 1 (full weight)
        weights = torch.ones_like(label)
        # lower certain weights based on the valid length of the protein
        weights = sequence_mask(weights, valid_len)
        self.reduction='none'
        unweighted_loss = super(MaskedSoftmaxCELoss, self).forward(pred.permute(0, 2, 1), label)

        weighted_loss = (unweighted_loss * weights).mean(dim=1)

        return weighted_loss


def get_baseline_data_from_data_iter(data_iter, device):
    """ Return a data_iter where the promoter-protein pairs are randomly shuffled.

    data_iter: data iter containing promoter-protein tensor samples.
               can be obtained from the load_dataset function of Seq2Seq_Train.py
    device: device the tensors are stored on.
    returns: a shuffled version of the data_iter where promoter-protein pairs
            are randomly shuffled. This data similates data if there was no
            promoter-protein relation whatsoever.
    """
    def shuffle_batch(batch):
        """ Encapsulated function to shuffle paired promoter-protein data.
            This 

        batch: [X, X_valid_len, Y, Y_valid_len]
        returns: batch but with X and X_valid_len shuffled with Y and Y_valid_len
        """
        X, X_valid_len, Y, Y_valid_len = [ii.to(device) for ii in batch]
        batch_size, sequence_length = X.shape

        # get randomized indexes for input
        batch_indexes = [index for index in range(batch_size)]
        random.shuffle(batch_indexes)

        # create empty tensor to store randomized input
        baseline_X = torch.empty(size=X.shape, dtype=X.dtype)
        baseline_X_valid_len = torch.empty(size=X_valid_len.shape, dtype=X_valid_len.dtype)

        # loop through shuffled indexes to create the baseline inputs
        for normal_index, shuffled_index in enumerate(batch_indexes):
            baseline_X[normal_index] = X[shuffled_index]
            baseline_X_valid_len[normal_index] = X_valid_len[shuffled_index]

        return baseline_X, baseline_X_valid_len, Y, Y_valid_len

    # apply the inner shuffle_batch function to each batch
    return [shuffle_batch(batch) for batch in data_iter]
    

def evaluate_network(net, dataset, device, loss_func, tgt_vocab):
    """

    net: the LSTM-LSTM network.
    dataset: the data iter to evaluate the network on.
    device: the device the tensors are stored on ('cpu')
    loss_func: the loss function used during training/to evaluate.
    tgt_vocab: <d2l vocab> vocabulary to translate protein tokens to amino acids.
            can be obtained through the get_protein_vocab() function from Seq2Seq_LoadData.py.
    
    returns: tuple with 3 items.
            1: <torch float> average loss
            2: <float> average accuracy
            3: <list of lists> where each list has the [correct prediction int, incorrect prediction int]
    """
    net.eval()

    # set empty variables to store statistics of all batches
    sum_normalized_loss = 0
    sum_pairwise_align_acc = 0
    total_correct_incorrect_per_position = None

    # evaluate batch by batch (to not overflow memory)
    for batch_nr, batch in enumerate(dataset):
        # get data for batch
        X, X_valid_len, Y, Y_valid_len = [ii.to(device) for ii in batch]
        if not total_correct_incorrect_per_position:
            # Y.shape[1] is the max protein length (so the index for the last possible position)
            # for each position track the [correct, incorrect] scores
            total_correct_incorrect_per_position = [[0,0] for position in range(Y.shape[1])]

        # get loss for this batch
        Y_hat = net(src=X.permute(1, 0), bos_token=tgt_vocab.token_to_idx['<bos>'], 
                    trg=Y.permute(1, 0), teacher_forcing_ratio=0.0)

        loss = loss_func(Y_hat.permute(1, 0, 2), Y, Y_valid_len)
        num_tokens = Y_valid_len.sum()
        normalized_loss = loss.sum()/num_tokens

        # get accuracy for this batch
        pairwise_alignment_acc = get_pairwise_alignment_accuracy(net=net, Y_pred=Y_hat, Y=Y, tgt_vocab=tgt_vocab)
        accuracies_over_length = get_accuracies_over_length(Y_pred=Y_hat, Y=Y, tgt_vocab=tgt_vocab)
        
        # add stats to totals
        sum_normalized_loss += normalized_loss
        sum_pairwise_align_acc += pairwise_alignment_acc
        for sequence_index, (correct, incorrect) in enumerate(accuracies_over_length):
            total_correct_incorrect_per_position[sequence_index][0] += correct
            total_correct_incorrect_per_position[sequence_index][1] += incorrect

    # average out loss and alignment accuracy
    return sum_normalized_loss/(batch_nr+1), sum_pairwise_align_acc/(batch_nr+1), total_correct_incorrect_per_position


def get_pairwise_alignment_accuracy(net, Y_pred, Y, tgt_vocab):
    """ Get pairwise alignment accuracy for a batch

    net: the LSTM-LSTM network to evaluate.
    Y_pred: [number of steps, batch size, target vocab size]
    Y: [batch size, number of steps]
    tgt_vocab: <d2l vocab> vocabulary to translate protein tokens to amino acids.
                can be obtained through the get_protein_vocab() function from Seq2Seq_LoadData.py.
            
    returns: <float> the average aligment accuracy
    """
    net.eval()
    aligner = Align.PairwiseAligner()

    Y_pred = Y_pred.permute(1, 0, 2)
    sum_align_percentage = 0

    for seq_index, predicted_seq in enumerate(Y_pred):
        # get known and predicted sequences
        predict_seq_tokens = predicted_seq.argmax(dim=1)
        predicted_seq_string = untokenize_sequence(predict_seq_tokens, vocab=tgt_vocab)
        true_seq_tokens = Y[seq_index]
        true_seq_string = untokenize_sequence(true_seq_tokens, vocab=tgt_vocab)

        # get alignment score
        if len(predicted_seq_string) < 1:
            align_percentage = 0
        else:
            align_score = aligner.score(predicted_seq_string, true_seq_string)
            align_percentage = align_score / max(len(predicted_seq_string), len(true_seq_string))    
        sum_align_percentage += align_percentage
        #print(predicted_seq_string, true_seq_string)
    return sum_align_percentage / (seq_index+1)


def get_accuracies_over_length(Y_pred, Y, tgt_vocab):
    """ Get the accurate/inaccurate prediction counts over the sequence length for a batch.

    Y_pred: [number of steps, batch size, target vocab size]
    Y: [batch size, number of steps]
    returns: [(correct <int>, incorrect <int>), ...]
    """
    Y = Y.permute(1, 0)
    Y_pred = Y_pred.argmax(dim=2)
    accuracy_list = list()

    pad_token = tgt_vocab.token_to_idx['<pad>']
    # for each sequence position in the length of predicted sequence
    for sequence_position, predicted_tokens in enumerate(Y_pred):
        match_count = 0
        error_count = 0

        # for each predicted token for this sequence position
        for epoch_nr, predicted_token in enumerate(predicted_tokens):
            correct_token =  Y[sequence_position][epoch_nr]
            # do not count padding tokens
            if correct_token == pad_token:
                continue
            # count matches
            elif predicted_token == correct_token:
                match_count += 1
            # count errors
            else:
                error_count += 1
        
        accuracy_list.append((match_count, error_count))

    return accuracy_list


def add_to_log(logfile_path, message, is_a_comment=False):
    """ Add a message to the logfile used during training.

    logfile_path: <str> path to the logfile to append to.
    message: <str> message to add to the logfile.
    is_a_comment: <bool> add a # infront of the message.
                 a # indicates the message does not describe epoch statistics.
    """
    now = datetime.now()
    full_message = f"{now.strftime(r'%Y_%m_%d_%Hh_%Mm_%Ss')}\t\t\t{message}\n"
    with open(logfile_path, 'a+') as f:
        if is_a_comment:
            f.write(f"#{full_message}")
        else:
            f.write(full_message)


def estimate_shannon_entropy(input_tensor):
    """  shannon entropy calculation adapted from https://onestopdataanalysis.com/shannon-entropy/
    A low shannon entropy (0.0) indicates low sequence complexity/diversity.
    A high shannon entopy (1.0) indicates high sequence diversity.

    input_tensor: Torch tensor with shape (batch size, sequence length, number of tokens)
    returns: <float> shannon entropy estimate between 0.0 and 1.0
    """
    batch_size = input_tensor.size()[0]
    number_of_tokens = input_tensor.size()[2]
    entropy_sum = 0
    # Go over all predicted sequences in the batch
    for predicted_sequence in input_tensor:
      tokens = predicted_sequence.argmax(dim=1).tolist()

      token_count = collections.Counter([tmp_token for tmp_token in tokens])

      # define distribution
      dist = [x/sum(token_count.values()) for x in token_count.values()]
      # use scipy to calculate entropy
      entropy_value = scipy_entropy(dist, base=number_of_tokens)
      entropy_sum += entropy_value

    # return the average entropy
    return entropy_sum / batch_size


def evaluate_seq_entropy_on_data_iter(net, data_iter, device, tgt_vocab):
    """ Estimate the average Shannon Entropy (sequence complexity) of the
    predicted proteins.

    net: the model which should predict the output proteins.
    data_iter: the data iter with the input promoter tensors.
               can be obtained from the load_dataset function of Seq2Seq_Train.py
    device: device the tensors are stored (default is 'cpu')
    tgt_vocab: <d2l vocab> vocabulary to translate protein tokens to amino acids.
              can be obtained through the get_protein_vocab() function from Seq2Seq_LoadData.py.

    returns: average entropy of all predicted protein sequences.
             Value of 1.0 are the most complex proteins possible.
             Value of ~0.1 are very repetitive protein sequences.
    """
    net.eval()
    total_entropy = 0
    for batch_nr, batch in enumerate(data_iter):
        net.eval()
        X, X_valid_len, Y, Y_valid_len = [ii.to(device) for ii in batch]
        Y_hat = net(src=X.permute(1, 0), bos_token=tgt_vocab.token_to_idx['<bos>'], 
            trg=Y.permute(1, 0), teacher_forcing_ratio=0.0)
        batch_entropy = estimate_shannon_entropy(Y_hat)
        total_entropy += batch_entropy

    return total_entropy / (batch_nr+1)



class CustomMSELoss(torch.nn.MSELoss):
    def __init__(self, compensate_rarity=None, compensate_biological_similarity=None, shannon_entropy_factor=None,
                 size_average=None, reduce=None, reduction = 'none') -> None:
        """ Copy of the original MSELoss class, with the forward function altered
        to ignore padding token by using the valid len tensor, compensate for amino acid frequency,
        and compensate for low entropy by adding the shannon's entropy.

        compensate_rarity: default is None, if to be used, give a dict with format:
            {<str> AA : <float> rarity_compensation_value}
            where 0 < rarity_compensation_value <=1.0 and a higher value
            means this AA accounts for more of the loss.
        compensate_biological_similarity: rudimentary variable, not implemented.
        shannon_entropy_factor: The factor of how much Shannon entropy contributes to the loss.
                                1.0 = in worst case scenario the Shannon entropy contributes
                                equally to the loss as the amino acid accuracy.
                                0.2 = the Shannon entropy contributes 5 times less to the loss
                                than the amino acid accuracy.
        """
        super().__init__(size_average, reduce, reduction)
        self.compensate_rarity = compensate_rarity
        self.compensate_biological_similarity = compensate_biological_similarity
        self.shannon_entropy_factor = shannon_entropy_factor


    def forward(self, input, target, valid_len):
        """
        input = tensor of floats with shape: (batch_size, num_steps, vocab_size)
        target = tensor of floats with shape: (batch_size, num_steps)
        valid_len = tensor of ints with shape: (batch_size)
        target_vocab = vocab with format {<str> AA : <int> token}

        returns: torch float of loss normalized for number of tokens
        """
        batch_size, num_steps, vocab_size = input.shape

        # must one hot encode the target, as it is has no vocab size dimension
        target = torch.nn.functional.one_hot(target, num_classes=vocab_size).float()

        # raw loss with shape (batch_size, num_steps, vocab_size)
        loss_func = torch.nn.MSELoss(reduction='none')
        loss = loss_func(input=input, target=target)

        # step 1) give padding tokens a loss of 0
        for seq_index, loss_per_seq in enumerate(loss):
            seq_valid_len = valid_len[seq_index]
            # if the sequence has padding
            if num_steps > seq_valid_len:
                padded_loss = torch.zeros((num_steps-seq_valid_len, vocab_size))
                # replace the padded part of the loss with zeros
                loss[seq_index][seq_valid_len:] = padded_loss
        # step 2) compensate loss based on amino acid occurence
        if self.compensate_rarity:
            for seq_index in range(loss.size(0)):
                loss_per_seq = loss[seq_index]
                # go over the loss for each token in the sequence
                for step_index in range(loss_per_seq.size(0)):
                    true_token = target[seq_index][step_index].argmax()
    
                    # find out how rare the target token is
                    relative_importance = self.compensate_rarity[int(true_token)]
                    importance_weights = torch.full((vocab_size,), fill_value=relative_importance)
                    #print(importance_weights)
                    loss[seq_index][step_index] = loss[seq_index][step_index] * importance_weights
                    #print(true_token, relative_importance, end="\t")

        if self.compensate_biological_similarity:
            "Not yet implemented"
        # step 3) compensate for sequence entropy
        
        if self.shannon_entropy_factor:
            batch_average_entropy = estimate_shannon_entropy(input)
            entropy_compensation = (1 - batch_average_entropy)*self.shannon_entropy_factor
            return loss.sum() / valid_len.sum() + entropy_compensation

        # lastly return the sum of the (compensated) loss, divided by total number of tokens
        return loss.sum() / valid_len.sum()


def train_seq2seq(net, train_iter, test_iter, tgt_vocab, device,
                  lr, num_epochs, loss_func, run_description = "no description given",
                  train_teach_force_ratio=0.0, max_norm_of_the_gradients=1,
                  evaluate_after_epochs=5, report_train_loss_after=5, save_network_after=50):
    """ Train a model for sequence to sequence promoter-protein predictions.

    net: the (untrained) LSTM-LSTM network to be trained.
    train_iter: The train iter to train the network on.
                Can be obtained from load_dataset function of Seq2Seq_LoadData.py
    test_iter: The test iter to evaluate the network on.
               Can be obtained from load_dataset function of Seq2Seq_LoadData.py     
    tgt_vocab: <d2l vocab> vocabulary to translate protein tokens to amino acids.
              can be obtained through the get_protein_vocab() function from Seq2Seq_LoadData.py.
    device: device the tensors are stored on (default='cpu')
    lr: <float> learning rate. The strength of the weight adjustments during training.
    num_epochs: <int> number of epochs the model is trained (how often the model passes through the training data)
    loss_func: the error/loss function used to train the model.
    
    optional:
    run_description: <str> the message to add to the logfile to describe what the run is for.
    train_teach_force_ratio: <float> the chance teacher forcing is used during training.\
                             a value of 0.4 means there is a 40% chance the decoder LSTM
                             uses the ground truth instead of its previous prediction during
                             training.
    max_norm_of_the_gradients: <float> gradient clipping value. The lower the value the more
                               the gradients are clipped.
    evaluate_after_epochs: <int> after this amount of epochs the logfile is updated with a model evaluation.
    report_train_loss_after: <int> number of batches untill the train_loss is printed in the terminal.
    save_network_after: <int> after how many epochs the model is stored.
    """
    # 1) initialize weights of net
    def xavier_init_weights(m):
        if type(m) == nn.Linear:
            nn.init.xavier_uniform_(m.weight)
        if type(m) == nn.LSTM or type(m) == nn.GRU:
            for param in m._flat_weights_names:
                if "weight" in param:
                    nn.init.xavier_uniform_(m._parameters[param])
    net.apply(xavier_init_weights)
    
    # 2) define model and optimizers
    net.to(device)
    optimizer = torch.optim.Adam(net.parameters(), lr=lr)

    # 3) create logfile for this run
    now = datetime.now()
    time_at_start = now.strftime(r'%Y_%m_%d_%Hh_%Mm_%Ss')
    logfile_path = f"./output/{time_at_start}_seq2seq_log.txt"
    model_report = "\n".join([f"#{line}" for line in repr(net).split("\n")])
    add_to_log(logfile_path=logfile_path, message=model_report, is_a_comment=True)
    add_to_log(logfile_path=logfile_path, message=f"description:{run_description}", is_a_comment=True)

    # 4) train loop
    for epoch in range(num_epochs):
        print(f"Start of epoch {epoch}:")
        # 4A) train in batches
        net.train()

        for batch_nr, batch in enumerate(train_iter):
            # Prepare tensors
            X, X_valid_len, Y, Y_valid_len = [ii.to(device) for ii in batch]

            # Predict and calculate loss,    y_hat = [prot_len, batch_size, vocab size]
            Y_hat = net(src=X.permute(1, 0), bos_token=tgt_vocab.token_to_idx['<bos>'],
                        trg=Y.permute(1, 0), teacher_forcing_ratio=train_teach_force_ratio)

            l = loss_func(Y_hat.permute(1, 0, 2), Y, Y_valid_len)
            l.sum().backward()  # Make the loss scalar for `backward`
        
            # trim excessive gradients, optimize weights
            d2l.grad_clipping(net, 1)
            torch.nn.utils.clip_grad_norm_(parameters=net.parameters(), max_norm=max_norm_of_the_gradients)
            optimizer.step()

            # print train loss
            if (batch_nr) % report_train_loss_after == 0:
                print(f"   Batch: {batch_nr}\t train_loss: {round(float(l.sum()), 5)}  Batch entropy {estimate_shannon_entropy(Y_hat)}")
            
                # show some train sequences:
                for seq_index, predicted_seq_tokens in enumerate(Y_hat.argmax(dim=2).permute(1, 0)):
                    predicted_seq = untokenize_sequence(predicted_seq_tokens, vocab=tgt_vocab)
                    print(f'   \tTrain example (first 20 AA):{predicted_seq[:20]}')
                    if seq_index > 1:
                        break

        # 4B) evaluate network performance
        net.eval()
        if epoch % evaluate_after_epochs == 0 or epoch == num_epochs-1:
            with torch.no_grad():
                normalized_train_loss, train_acc, train_acc_over_len = evaluate_network(net=net, dataset=train_iter, device=device, loss_func=loss_func, tgt_vocab=tgt_vocab)
                # create baseline from test data
                baseline_iter = get_baseline_data_from_data_iter(data_iter=test_iter, device=device)
                normalized_baseline_loss, baseline_acc, baseline_acc_over_len = evaluate_network(net=net, dataset=baseline_iter, device=device, loss_func=loss_func, tgt_vocab=tgt_vocab)
                normalized_test_loss, test_acc, test_acc_over_len =  evaluate_network(net=net, dataset=test_iter, device=device, loss_func=loss_func, tgt_vocab=tgt_vocab)
                evaluate_message = f"Epoch {epoch}\tTrain_loss {normalized_train_loss}\tTrain_acc {train_acc}\tTrain_acc_over_len {train_acc_over_len}\t" + \
                                   f"Base_loss {normalized_baseline_loss}\tBase_acc {baseline_acc}\tBase_acc_over_len {baseline_acc_over_len}\t" + \
                                   f"Test_loss {normalized_test_loss}\tTest_acc {test_acc}\tTest_acc_over_len {test_acc_over_len}\t" + \
                                   f"Test_entropy {evaluate_seq_entropy_on_data_iter(net=net, data_iter=test_iter, device=device, tgt_vocab=tgt_vocab)}"

                add_to_log(logfile_path=logfile_path, message=evaluate_message)
                
        if epoch % save_network_after == 0 and epoch != 0 or epoch == num_epochs-1:
            save_path = f"./output/{time_at_start}_Epoch{epoch}.pth"
            torch.save(net, save_path)
    # return the trained net
    return net


def generate_artificial_data_thesis():
    """ Train 10 seq2seq models on art data. To get an average accuracy over 10 runs.
        Do this for teach_force = 0.0 (basis) and teach force = 0.2
        This function was called to generate the artificial LSTM-LSTM model data (figure 3 in the thesis).
    """
    batch_size = 32
    dataset_file = r"datasets\prom2prot\ARTIFICIAL_2022_02_21_09h_16m_34s__promoterlen50_patternlen_6.txt" # Can reach acc 1.0 (100 epochs)
    tgt_vocab = get_protein_vocab(add_asterisk_to_vocab=True, use_selenocysteine=False)
    src_vocab = get_promoter_vocab()
    train_iter, test_iter = load_dataset(dataset_path=dataset_file, src_vocab=src_vocab, tgt_vocab=tgt_vocab, batch_size=batch_size)
    max_num_of_threads = 6
    loss_class = CustomMSELoss(compensate_rarity=None, compensate_biological_similarity=None,
                                   shannon_entropy_factor=None)
    torch.set_num_threads(max_num_of_threads)

    # define model
    device = 'cpu'
    
    LR = 0.0005  # article used decreasing learning rate.
    NUM_EPOCHS = 100
    TEACHER_FORCE = 0.2
    for REPEAT in range(10):
        model = get_LSTM_LSTM_model()
        model = train_seq2seq(net=model, train_iter=train_iter, test_iter=test_iter, tgt_vocab=tgt_vocab,
                            device=device, lr=LR, num_epochs=NUM_EPOCHS, loss_func=loss_class, 
                            evaluate_after_epochs=5, save_network_after=100, train_teach_force_ratio=TEACHER_FORCE,
                            run_description=f"Artificial data seq2seq: Repeat {REPEAT}   Teach force {TEACHER_FORCE}")    
        

def generate_ppd_data_thesis():
    """ Train 10 LSTM-LSTM models on the PPD dataset.
        This function was called to generate the data for figure 4 in the thesis.
        Try to predict proteins using a promoter input, resulted in repetitive ALALALA protein sequences.
    """
    src_vocab = get_promoter_vocab()
    tgt_vocab = get_protein_vocab(add_asterisk_to_vocab=True, use_selenocysteine=False)
    dataset_file = r"datasets\prom2prot\2022_03_22_08h_57m_01s__PPD_maxproteinlength_None__MaxPerIdt0.5.txt" 
    max_num_of_threads = 10
    torch.set_num_threads(max_num_of_threads)
    device = 'cpu'
    NUM_EPOCHS = 100
    BATCH_SIZE = 32
    LEARNING_RATE = 0.0005
    TEACHER_FORCE = 0.2
    criterion = CustomMSELoss(compensate_rarity=None, compensate_biological_similarity=None,
                                   shannon_entropy_factor=None)

    for REPEAT in range(0, 10):
        train_iter, test_iter = load_dataset(dataset_path=dataset_file, src_vocab=src_vocab, tgt_vocab=tgt_vocab,
                                             batch_size=BATCH_SIZE)
        model = get_LSTM_LSTM_model()
        model = train_seq2seq(net=model, train_iter=train_iter, test_iter=test_iter,
                            tgt_vocab=tgt_vocab, device=device, lr=LEARNING_RATE,
                            num_epochs=NUM_EPOCHS, loss_func=criterion, evaluate_after_epochs=10,
                            save_network_after=100, train_teach_force_ratio=TEACHER_FORCE,
                            run_description=f"Generate PPD data for thesis. Repeat:{REPEAT}")    


def generate_diverse_ppd_data_thesis():
    """ Train 5 LSTM-LSTM models while punishing repetitive sequences using Shannon's entropy.
        This function was used to generate the data for figure 5 in the thesis.
        Resulted in an increased diversity of the first 20 amino acids, after which
        the models predicted repetitive proteins.
    """
    src_vocab = get_promoter_vocab()
    tgt_vocab = get_protein_vocab(add_asterisk_to_vocab=True, use_selenocysteine=False)
    dataset_file = r"datasets\prom2prot\2022_03_22_08h_57m_01s__PPD_maxproteinlength_None__MaxPerIdt0.5.txt" 
    max_num_of_threads = 12
    torch.set_num_threads(max_num_of_threads)
    device = 'cpu'

    NUM_EPOCHS = 100
    BATCH_SIZE = 32
    LEARNING_RATE = 0.00001
    TEACHER_FORCE = 0.4
    SHANNON_ENTROPY_FACTOR = 2

    criterion = CustomMSELoss(compensate_rarity=None, compensate_biological_similarity=None,
                                   shannon_entropy_factor=SHANNON_ENTROPY_FACTOR)

    for REPEAT in range(0, 5):
        model = get_LSTM_LSTM_model()
        train_iter, test_iter = load_dataset(dataset_path=dataset_file, src_vocab=src_vocab, tgt_vocab=tgt_vocab,
                                             batch_size=BATCH_SIZE)
        model = train_seq2seq(net=model, train_iter=train_iter, test_iter=test_iter,
                                tgt_vocab=tgt_vocab, device=device, lr=LEARNING_RATE,
                                num_epochs=NUM_EPOCHS,
                                loss_func=criterion, evaluate_after_epochs=10,
                                save_network_after=100,
                                train_teach_force_ratio=TEACHER_FORCE,
                                run_description=f"Generate PPD data for thesis. Repeat:{REPEAT}")    


def run_main():
    """
    """
    # Final:
    # generate_artificial_data_thesis()
    # generate_ppd_data_thesis
    # generate_diverse_ppd_data_thesis # with entropy


if __name__ == "__main__":
   """
   """
   run_main()

