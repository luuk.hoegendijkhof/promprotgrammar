""" Contains functions to generate artificial promoter-protein domain data used to 
validate the CNN-LSTM (DeePromoter) deep learning models. 

Luuk Hoegen Dijkhof 2022 jan 24
Wageningen University BIF group
"""
import random
from datetime import datetime
from os import path


def get_artificial_promoter(total_len, pattern):
    """ This function produces an artificial promoter sequence.

    total_len: <int> desired length of the artificial promoter string.
    pattern: <str> a nucleotide pattern which will be placed randomly anywhere in the promoter sequence.
    returns: <str> artificial promoter sequence which contains the given pattern at a random position.
    """
    pattern = pattern.upper()
    if len(pattern) > total_len:
      raise ValueError("WRONG INPUT. Pattern length must be smaller than total length.")
    
    random_seq = ''.join(random.choice('CGTA') for _ in range(total_len- len(pattern)))
    pattern_start = random.randrange(total_len)
    return random_seq[:pattern_start] + pattern + random_seq[pattern_start:]


def generate_data_file(target_folder, prom_len, total_samples=500, train_test_ratio=0.5):
    """ Create an artificial promoter-protein domain dataset file.

    target_folder: <str> The folder in which the new dataset file should be created.
    prom_len: <int> The total length (nucleotides) of the artificial promoters
    
    [optional]
    total_samples: <int> automatically 500, total number of generated prom-prot pairs.
    train_test_ratio: <float> automatically 0.5, the ratio of train to test samples
    returns: True (if succesful)
    """
    # Setup file names and empty file
    if not path.exists(target_folder):
        raise ValueError("Target folder does not exist! Please create the target folder.")
    runtime_start = datetime.now().strftime(r'%Y_%m_%d_%Hh_%Mm_%Ss')

    pattern_domain_dict = {
    1: ("ACGATC", ("Domain1", "Domain5") ),
    2: ("GCTTCC", ("Domain2", )          ),
    3: ("CACGGG", ("Domain3", )          ),
    4: ("TTTGGA", ("Domain4", ),         ),
    5: ("GATCAG", ("Domain5", "Domain3") )}
    

    file_name = f"{runtime_start}__promoterlen{prom_len}_number_of_patterns{len(pattern_domain_dict)}.txt"
    
    # create file and write to it
    with open(f'{target_folder}/{file_name}', 'a') as dataset_file:
        # add documentation to the dataset file
        dataset_file.write('#A tab delimited file containing artificial promoter-domain pairs\n')
        dataset_file.write('#Sample_index\tSample_type\tProm_seq\tDomains\n')   
        # add promoter-protein data to the file
        for sample_index in range(0, total_samples):
            # if a train sample
            if random.random() <= train_test_ratio:
                type = "Train"
            else:
                type = "Test"

            pattern_number = random.randrange(1, 6)
            pattern, domain_list = pattern_domain_dict[pattern_number] 
            promoter = get_artificial_promoter(total_len=prom_len, pattern=pattern)
            # add sample to file
            domain_string = " ".join(domain_list)
            if len(domain_list) == 1:
                domain_string = str(domain_list[0])
            dataset_file.write(f'{sample_index}\t{type}\t{promoter}\t{domain_string}\n')
    
    return True


if __name__ == "__main__":
    generate_data_file(target_folder=r"./", prom_len=50, total_samples=1000, train_test_ratio=0.5)
