""" Contains functions to generate artificial promoter-protein data used to 
valid Seq2seq deep learning models. 

Luuk Hoegen Dijkhof 2022 jan 24
Wageningen University BIF group
"""
import random
from datetime import datetime
from os import path


def get_artificial_promoter(total_len, pattern):
    """ This function produces an artificial promoter sequence.

    total_len: <int> desired length of the artificial promoter string.
    pattern: <str> a nucleotide pattern which will be placed randomly anywhere in the promoter sequence.
    returns: <str> artificial promoter sequence which contains the given pattern at a random position.
    """
    pattern = pattern.upper()
    if len(pattern) > total_len:
      raise ValueError("WRONG INPUT. Pattern length must be smaller than total length.")
    
    random_seq = ''.join(random.choice('CGTA') for _ in range(total_len- len(pattern)))
    pattern_start = random.randrange(total_len)
    return random_seq[:pattern_start] + pattern + random_seq[pattern_start:]


def get_random_protein_seq(total_len):
    """ This function returns a fully random protein sequence.

    total_len: <int> the desired length of the random protein string.
    returns: <str> a protein string formatted as fully randomized 1-letter amino acid codes
    """
    return ''.join(random.choice('ARNDCQEGHILKMFPSTWYV') for _ in range(total_len))


def generate_data_file(target_folder, prom_len, prot_len, prom_A_pattern,
                       prom_B_pattern, prot_A_seq, prot_B_seq, total_samples=500, train_test_ratio=0.5):
    """ Create an artificial promoter-protein tab delimeted datafile with certain patterns.

    target_folder: <str> The folder in which the new dataset file should be created.
    prom_len: <int> The total length (nucleotides) of the artificial promoters
    prot_len: <int> The total length (amino acids) of the artificial proteins
    prom_A_pattern: <str> The nucleotide pattern in the promoter associated with the 'A' protein
    prom_B_pattern: <str> The nucleotide pattern in the promoter associated with the 'B' protein
    prot_A_seq: <str> The amino acid sequence of the 'A' protein
    prot_B_seq: <str> The amino acid sequence of the 'B' protein
    
    [optional]
    total_samples: <int> automatically 500, total number of generated prom-prot pairs.
    train_test_ratio: <float> automatically 0.5, the ratio of train to test samples
    returns: True (if succesful)
    """
    # Setup file names and empty file
    if not path.exists(target_folder):
        raise ValueError("Target folder does not exist! Please create the target folder.")
    runtime_start = datetime.now().strftime(r'%Y_%m_%d_%Hh_%Mm_%Ss')
    file_name = f"{runtime_start}__promoterlen{prom_len}_patternlen_{max(len(prom_A_pattern), len(prom_B_pattern))}.txt"
    
    # create file and write to it
    with open(f'{target_folder}/{file_name}', 'a') as dataset_file:
        # add documentation to the dataset file
        dataset_file.write( '#A tab delimited file containing artificial promoter-protein pairs\n')
        dataset_file.write(f'#The promoter pattern associated with protein A: {prom_A_pattern}\n')
        dataset_file.write(f'#The protein A sequence: {prot_A_seq}\n')
        dataset_file.write(f'#The promoter pattern associated with protein B: {prom_B_pattern}\n')
        dataset_file.write(f'#The protein A sequence: {prot_B_seq}\n')
        dataset_file.write(f'#Sample_index\tSample_type\tProm_seq\tProt_seq\n')
        
        # add promoter-protein data to the file
        for sample_index in range(0, total_samples):
            # if a train sample
            if random.random() <= train_test_ratio:
                type = "Train"
            else:
                type = "Test"

            # generate a protein A sample
            if random.random() <= 0.5: 
                prom = get_artificial_promoter(total_len=prom_len, pattern=prom_A_pattern)
                prot = prot_A_seq
            # generate a protein B sample
            else:
                prom = get_artificial_promoter(total_len=prom_len, pattern=prom_B_pattern)
                prot = prot_B_seq
            
            # add sample to file
            dataset_file.write(f'{sample_index}\t{type}\t{prom}\t{prot}\n')
    
    return True


if __name__ == "__main__":
    # Simple dataset, used checking if the model can learn patterns in longer (50bp) promoters.
    """
    generate_data_file(target_folder=r"./ignore_folder/datasets", prom_len=50, prot_len=8, prom_A_pattern="AGAGCT",
                       prom_B_pattern="CCGGAT", prot_A_seq="KERSTKERST", prot_B_seq="FEESTFEEST")
    """
    # Artificial dataset, makes sure that the model does not learn if there is no pattern
    """
    generate_data_file(target_folder=r"./ignore_folder/datasets", prom_len=50, prot_len=8, prom_A_pattern="A",
                       prom_B_pattern="G", prot_A_seq="AAAAAAAA", prot_B_seq="TTTTTTTT")
    """

    # artificial dataset, long target proteins to show the decoder can predict long proteins
    """
    generate_data_file(target_folder=r"./", prom_len=15, prot_len=54, prom_A_pattern="AGAGCTG",
                       prom_B_pattern="CCGGATC",
                       prot_A_seq="KERSTKERSTTESTTESTTESTTESTTESTTESTTESTKERSTKERSTHALLQ",
                       prot_B_seq="FEESTFEESTCCCAAAVVVLLLCCCVVVLLLAAACCCLLLVVVLLLAAALHQI")
    """