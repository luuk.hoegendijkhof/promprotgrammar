""" This file contains the functions to evaluate the performance of the CNN-LSTM
domain classifier on the dataset.
"""
import csv
import torch
from DeePromoterLoadData import get_total_domains_in_dataset, load_dataset, PromDomainDataset, parse_dataset, get_baseline_data_from_data_iter
import os


def get_net_performance_statistics(test_iter, net, domain_to_token_dict):
    """ This function evaluates the peformance of a trained CNN-LSTM by calculating
    the TP, FP, FN, TN statistics for each domain on a given test data iter. 

    test_iter: the test data iter obtained through the DeePromoterLoadData.py file
    net: the trained CNN-LSTM (DeePromoter) model.
    domain_to_token_dict: <dict> with {<str> domain name: <int> domain token index}

    returns: an output dict with a dictionary inside for each domain with format:
        {<int> domain index: {"Name":domain_name, "TP":number of true positives,
                             "FP":number of false positives, "FN":number of false negatives, "TN":number of true negatives}
        }
    """
    output_dict = dict()
    for temp_domain_name, temp_domain_token in domain_to_token_dict.items():
        # Create an output dict to store true positive, false positive, false negative, true negative
        output_dict[temp_domain_token] = {"Name":temp_domain_name, "TP":0, "FP":0, "FN":0, "TN":0}
        

    for X, Y in test_iter:
        Y_pred = torch.round(net(X))  # Y_pred shape: (batch size, number of domains) 1.0=positive prediction, 0.0=negative prediction
        correct = Y == Y_pred   # correct shape: (batch size, number of domains) True or False

        # Go over each batch index in the test iter
        for batch_index in range(len(Y_pred)):
            # For each predicted token (domain)
            for domain_token in range(len(output_dict)):
                # positive prediction
                if Y_pred[batch_index][domain_token] == 1.0:
                    if correct[batch_index][domain_token]:
                        output_dict[domain_token]['TP'] += 1     # true positive
                    else:
                        output_dict[domain_token]['FP'] += 1     # false positive
                # negative prediction
                else:
                    if correct[batch_index][domain_token]:
                        output_dict[domain_token]['TN'] += 1     # true negative
                    else:
                        output_dict[domain_token]['FN'] += 1     # false negative
    
    return output_dict


def get_network_performance(net_path_list, dataset_path, get_baseline=False):
    """ This function evaluates multiple (or one) CNN-LSTM training sessions to get a general
    overview of the model performance in classifying each protein domain.

    net_path_list: list of paths, where each path has the format:
                   <str> path to the stored CNN-LSTM file (ends with .pth)
    dataset_path: <str> path to the dataset used to train the model.
    get_baseline: <bool> <optional> get a baseline evaluation. This means the
                  promoter-protein domain pairs will be shuffled randomly in each batch.
                  This can be used to evaluate how well the model performs on the data if
                  there is no promoter-protein domain relation. 

    returns: an output dict with a dictionary inside for each domain with format:
        {<int> domain index: {"Name":domain_name, "TP":number of true positives,
                             "FP":number of false positives, "FN":number of false negatives, "TN":number of true negatives}
        }
    """
    max_prom_len = None

    with torch.no_grad():

        all_performance_dict = dict()

        for net_path in net_path_list:

            net = torch.load(net_path)

            # Get data
            dataset = PromDomainDataset(sample_list=parse_dataset(dataset_path=dataset_path),
                                        max_prom_len=max_prom_len)
            train_iter, test_iter = load_dataset(dataset_path, batch_size=12, max_prom_len=None)

            if get_baseline:
                test_iter = get_baseline_data_from_data_iter(data_iter=test_iter, device='cpu')
     
            domain_to_token_dict = dataset.get_domain_token_dict(sample_list=parse_dataset(dataset_path))

            # Get performance of this network on the test data
            net_performance_dict = get_net_performance_statistics(test_iter=test_iter, net=net,
                                    domain_to_token_dict=domain_to_token_dict)

            for token_index, value_dict in net_performance_dict.items():
                
                if not token_index in all_performance_dict:
                    all_performance_dict[token_index] = value_dict
                    continue
                all_performance_dict[token_index]["TP"] += value_dict["TP"]
                all_performance_dict[token_index]["FP"] += value_dict["FP"]
                all_performance_dict[token_index]["TN"] += value_dict["TN"]
                all_performance_dict[token_index]["FN"] += value_dict["FN"]
    
    return all_performance_dict


def create_performance_csv(csv_path, all_performance_dict):
    """ Creates a csv file with the CNN-LSTM performance statistics obtained from
    a performance dict.

    csv_path: <str> path to where the new .csv file will be created containing the model performance statistics.
    all_performance_dict: <dict> dictionary with model performance statistics obtained
                         from the 'get_network_performance' function above.
    """
    while os.path.exists(csv_path):
        print(f"Path already exists!: {csv_path}")
        csv_path = csv_path.replace(".csv", "_(1).csv")
        print(f"Target file renamed to {csv_path}")

    with open(csv_path, 'w', encoding="UTF8", newline='') as output_csv:
        # create CSV writer class
        csv_writer = csv.writer(output_csv)
        
        # Create header line
        header_list = ['Domain_token_index', 'Domain_name', "TP", "FP", "TN", "FN",
                       "Sensitivity", "Specificity", "PPV", "NPV"]
        csv_writer.writerow(header_list)
        
        # For each domain
        for domain_token, domain_stats in all_performance_dict.items():
            # calculate extra statistics
            sensitivity = 0.0
            if domain_stats["TP"] > 0:
                sensitivity = domain_stats["TP"] / (domain_stats["TP"] + domain_stats["FN"])
            specificity = 0.0
            if domain_stats["TN"] > 0:
                specificity = domain_stats["TN"] / (domain_stats["TN"] + domain_stats["FP"])
            ppv = 0.0
            if domain_stats["TP"] > 0:
                ppv = domain_stats["TP"] / (domain_stats["TP"] + domain_stats["FP"])
            npv = 0.0
            if domain_stats["TN"] > 0:
                npv = domain_stats["TN"] / (domain_stats["FN"] + domain_stats["TN"])

            # write statistics to csv file
            info_list = [domain_token, domain_stats['Name'], domain_stats['TP'], domain_stats['FP'],
                        domain_stats['TN'], domain_stats["FN"], sensitivity, specificity, ppv, npv]
            csv_writer.writerow(info_list)


def get_promoter_domain_pairs_from_dataset(path_to_dataset):
    """ Get the sample index for each promoter and which domains are paired to this promoter.

    path_to_dataset: <str>
    returns: <dict> {<int>:Sample_index : <set> {<str> domain1_name, <str> domain2_name, ...}}
    """
    # 1) Get stats
    prom_domain_dict = dict()
    with open(path_to_dataset, 'r') as data_file:
        for line in data_file.readlines():
            line = line.strip()
            if line.startswith("#Sample_index"):
                line = line.replace("#", "")
                headers = line.split("\t")
                sample_nr_index = headers.index("Sample_index")
                domain_list_index = headers.index("Domains")
                continue

            elif line.startswith("#"):
                continue

            else:
                line_info = line.split("\t")
                domain_string = line_info[domain_list_index]
                domain_set = set(domain_string.split(" "))
                prom_domain_dict[int(line_info[sample_nr_index])] = domain_set

    return prom_domain_dict


def create_dataset_statistics_files(prom_domain_dict, target_proms_per_domain_csv, target_domains_per_prom_csv):
    """ Create 2 .csv files showing the data distributions of a dataset.

    prom_domain_dict: a dictionary with format {<int>:Sample_index : <set> {<str> domain1_name, <str> domain2_name, ...}}
                      this dictionary can be obtained using the 'get_promoter_domain_pairs_from_dataset' function.
    target_proms_per_domain_csv: <str> path to the desired file containing the promoters per domain statistics.
    target_domains_per_prom_csv: <str> path to the desired file containing the domains per promoter statistics.
    """
    # 0) Initiate empty dict to count domain occurence
    domain_occurence = dict()

    # A) Write promoter -> domain stats csv
    with open(target_domains_per_prom_csv, 'w', encoding="UTF8", newline='') as domains_per_prom:
        # create CSV writer class
        csv_writer = csv.writer(domains_per_prom)
        header_list = ['Promoter_sample_index', "Number_of_domains"]
        csv_writer.writerow(header_list)

        for promoter_sample_index, domain_list in prom_domain_dict.items():
            csv_writer.writerow([promoter_sample_index, len(domain_list)])
            
            for domain in domain_list:
                if not domain in domain_occurence:
                    domain_occurence[domain] = 0
                domain_occurence[domain] += 1

    # B) Write domains -> promoter stats csv
    with open(target_proms_per_domain_csv, 'w', encoding="UTF8", newline='') as proms_per_domain:
        # create CSV writer class
        csv_writer = csv.writer(proms_per_domain)
        header_list = ['Domain_name', "Occurence_count"]
        csv_writer.writerow(header_list)

        for domain_name, occurence_count in domain_occurence.items():
            csv_writer.writerow([domain_name, occurence_count])


if __name__ == "__main__":
    # Read a dataset file and get promoter-domain ratio and domain-promoter ratio
    dataset_stats = get_promoter_domain_pairs_from_dataset(r"datasets\prom2domain\ppd_dataset_minpromlen20_maxutrlen200_max_evalue0.0001_maxoverlap0.5_minoccurence25.txt")
    create_dataset_statistics_files(prom_domain_dict=dataset_stats,
                                    target_domains_per_prom_csv="./domains_per_prom.csv",
                                    target_proms_per_domain_csv="./proms_per_domain.csv")



    # Get performance of the 5 fully trained CNN-LSTM models
    path_start = r"results\output_and_saved_models\result_PPD_domainclassifier_5runs"
    path_list = [f"{path_start}/2022_05_03_19h_58m_03s_.pth",
                f"{path_start}/2022_05_03_20h_00m_13s_.pth",
                f"{path_start}/2022_05_03_20h_02m_27s_.pth",
                f"{path_start}/2022_05_03_20h_04m_42s_.pth",
                f"{path_start}/2022_05_03_20h_06m_53s_.pth",
                ]
    network_performance_dict = get_network_performance(dataset_path=r"datasets\prom2domain\ppd_dataset_minpromlen20_maxutrlen200_max_evalue0.0001_maxoverlap0.5_minoccurence25.txt",
                                                        net_path_list=path_list,
                                                        get_baseline=False)
    create_performance_csv(csv_path=f'./5_classifiers_performance.csv',
                          all_performance_dict=network_performance_dict)
