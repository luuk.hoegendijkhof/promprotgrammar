# For the LSTM-LSTM create a plot which shows the average accuracy of 10 runs.
# Also show the standard deviation as a lightblue surrounding.

# Import the data and look at the first six rows
data <- read.csv(file = './result_artificial_seq2seq_10_runs/seq2seq_artificial_10runs_accuracy_comparison_with_teach_force.csv')

get_epoch <- function(epoch_column_string)
  {
  epoch_part = unlist(strsplit(epoch_column_string, '_'))[1]
  epoch = gsub("Epoch", "", epoch_part)
  return(strtoi(epoch))
}

# Create mean
epoch_data <- data[, 3:ncol(data)]
mean_accuracies = colMeans(epoch_data)
standard_deviations = sapply(epoch_data, sd)
x_indexes <- sapply(names(data)[3:ncol(data)], get_epoch)

#determine error band (upper and lower)
psd<-mean_accuracies+standard_deviations
nsd<-mean_accuracies-standard_deviations

plot(x_indexes, mean_accuracies, ty="l", col="blue", 
     ylab="Sequence identity accuracy", 
     xlab='Epochs',
     lty=1,lwd=3)

#draw boundary and fill
lines(x_indexes, psd)
lines(x_indexes, nsd)
polygon(x=c(x_indexes, rev(x_indexes)), y=c(psd, rev(nsd)), col="lightblue", density = 40, angle=90)
#redraw line on top
lines(x_indexes, mean_accuracies, col="blue",lwd=3)