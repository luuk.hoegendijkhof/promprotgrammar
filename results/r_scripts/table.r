# Create a table to compare the performance of the CNN-LSTM per protein domain.


#install.packages("formattable")
library(formattable)

data <- read.csv(file = "./result_PPD_domainclassifier_5runs/classifier_final_5runs.csv")
data$Sensitivity=format(round(data$Sensitivity, digits=2), nsmall=2)
data$Specificity=format(round(data$Specificity, digits=2), nsmall=2)
data$PPV = round(data$PPV, digits=2)
data$NPV = round(data$NPV, digits=2)
data_shown = subset(data, PPV != 0.0)
data_shown = subset(data_shown, NPV != 0.0)
data_shown$Domain_token_index <- NULL
data_shown = data_shown[order(-data_shown$PPV), ]
#data_shown = data_shown[0:5, ] # show best 5
row.names(data_shown) <- NULL
formattable(data_shown, 
            align =c("l","c","c","c","c", "c", "c", "c", "c"),
            list=c()
           ) 

#list("D" = formatter(
#  "span", style = ~ style(color = "grey",font.weight = "bold")
