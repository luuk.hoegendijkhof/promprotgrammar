# Create a plot to show the domain stats of the dataset.
# how many promoters does each PFAM domain have?
# how many PFAM domains does each promoter have?

library(ggplot2)
data <- read.csv(file = "./result_PPD_domainclassifier_5runs/proms_per_domain.csv")
data = data[order(-data$Occurence_count), ]
data$Domain_name = factor(data$Domain_name, levels=data$Domain_name[order(-data$Occurence_count)])

# Basic line plot with points
ggplot(data=data,
       asp=1,
       aes(x=c(1:length(Domain_name)),
           y=Occurence_count),
       ) +

      xlab('Domain') +
      ylab('Number of promoters') +
      geom_line(size=1, color='blue')+
      #geom_point() + 
     theme(axis.title.y = element_text(margin = margin(t = 10, r = 10, b = 0, l = 0)),
           aspect.ratio=5/5)


data2 <- read.csv(file = "./result_PPD_domainclassifier_5runs/domains_per_prom.csv")
data2 = data2[order(-data2$Number_of_domains), ]

ggplot(data=data2,
       asp=1,
       aes(x=c(1:length(Promoter_sample_index)),
           y=Number_of_domains),
) +
  xlab('Promoter') +
  ylab('Number of domains') +
  geom_line(size=1, color='blue')+
  ylim(0, 4)+
  #geom_point() + 
  theme(axis.title.y = element_text(margin = margin(t = 20, r = 20, b = 0, l = 0)),
        aspect.ratio=5/6)

mean(data2$Number_of_domains)