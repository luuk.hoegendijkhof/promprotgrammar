# Create two plots. Show how the CNN-LSTM predicts domains compared to random.
# Plot the sensitivity compared to random and the positive predictive value (PPV) compared to random.

data <- read.delim('./result_PPD_domainclassifier_5runs/DomainPredictionBetterThanRandom.txt')
library(ggplot2)

ggplot(data, aes(Domain_token_index, Sensitivity)) + 
  geom_point(aes(x=reorder(Sensitivity, -PPV),
                 colour = cut(Sensitivity, c(-Inf, 0.001, 0.0011, Inf))),
             size = 3) +
  geom_hline(yintercept=0.0)+ 
  theme(text = element_text(size = 16)) +
  scale_color_manual(name = "Improved sensitivity",
                     values = c("(-Inf,0]" = "red",
                                "(0,0.001]" = "red",
                                "(0.0011, Inf]" = "green"),
                     labels = c("", "", ""))


ggplot(data, aes(Domain_token_index, PPV)) + 
  geom_point(aes(x=reorder(PPV, -PPV),
                 colour = cut(PPV, c(-Inf, 0.001, 0.0011, Inf))),
             size = 3) +
  geom_hline(yintercept=0.0)+ 
  theme(text = element_text(size = 16)) +
  scale_color_manual(name = "Improved PPV",
                     values = c("(-Inf,0]" = "red",
                                "(0,0.001]" = "red",
                                "(0.0011, Inf]" = "green"),
                     labels = c("", "", ""))
  