""" This file contains the functions to load data for the sequence-to-sequence
LSTM-LSTM model (seq2seq). It includes functions to encode a promoter/protein to a pytorch tensor,
load a promoter-protein dataset and a custom pytorch Dataset class used to generate
train and test iters for the seq2seq models.
"""
from d2l import torch as d2l
import torch


class PromProtDataset(torch.utils.data.Dataset):
    """ A custom pytorch dataset class made for this thesis.
        This dataset can be used to generate train and test data iters for 
        the sequence-to-sequence (seq2seq) LSTM-LSTM.
    """
    def __init__(self, sample_list, src_vocab, tgt_vocab, largest_promoter_length, largest_protein_length) -> None:
        """ This function initiates the PromProt class and its values.

        sample_list: list of all samples as tuples with format 
                    (<int> sample index, <str> 'Train' or 'Test', <str> promoter sequence, <str> protein sequence)
        src_vocab: <d2l vocab> vocabulary to translate promoter tokens to nucleotides.
                can be obtained through the get_promoter_vocab() function.
        tgt_vocab: <d2l vocab> vocabulary to translate protein tokens to amino acids.
                can be obtained through the get_protein_vocab() function.
        largest_promoter_length: <int> the length of the longest promoter in the dataset.
        largest_protein_length: <int> the length of the longer protein in the dataset.
        """
        self.src_vocab = src_vocab
        self.tgt_vocab = tgt_vocab
        self.largest_promoter_length = largest_promoter_length
        self.largest_protein_length = largest_protein_length
        self.prom_prot_dict = self.create_prom_prot_dict(sample_list=sample_list)


    def __len__(self):
        """ returns how many samples are in the dataset
        """
        return len(self.prom_prot_dict)
    

    def __getitem__(self, idx):
        """ This function returns a tuple based on the index provided.
        Each tuple contains 4 tensors: 
            A promoter tensor with shape: [batch size, number of nucleotide tokens, longest promoter length]
            A promoter length tensor with shape: ['batch size']
            A protein tensor with shape: [batch size, number of amino acid tokens, longest protein length]
            A protein length tensor with shape: ['batch size']
        """
        return self.prom_prot_dict[idx]
    

    def create_prom_prot_dict(self, sample_list):
        """ This function generates a dict with indexes as key and a list of tensors as value.
            Each index has a tensor for the promoter, tensor for the valid promoter length
            and a tensor for the protein and a tensor for the protein valid length. The valid
            length tensors are required for the custom loss functions used. 

        sample_list: list of all samples as tuples with format 
            (<int> sample index, <str> 'Train' or 'Test', <str> promoter sequence, <str> protein sequence)

        returns:
            a dictionary with an index (integer) key and a tuple value.
            the tuple contains 4 tensors:
                A promoter tensor with shape: [batch size, number of nucleotide tokens, longest promoter length]
                A promoter length tensor with shape: ['batch size']
                A protein tensor with shape: [batch size, number of amino acid tokens, longest protein length]
                A protein length tensor with shape: ['batch size']
        """
        prom_prot_dict = dict()
        for sample_index, sample_type, prom_seq, prot_seq in sample_list:
            # +2 to add the <bos> token and <eos> token to the valid length
            valid_prom_len = torch.tensor(len(prom_seq)+2).type(torch.LongTensor)
            valid_prot_len = torch.tensor(len(prot_seq)+2).type(torch.LongTensor)
            # num_steps is the largest protein length +2 so each promoter tensor has the same shape
            prom_seq_tokenized = tokenize_sequence(input_seq=prom_seq, vocab=self.src_vocab, num_steps=self.largest_promoter_length+2)
            prom_tensor = torch.tensor(prom_seq_tokenized).type(torch.LongTensor)
            
            prot_seq_tokenized = tokenize_sequence(input_seq=prot_seq, vocab=self.tgt_vocab, num_steps = self.largest_protein_length+2)
            prot_tensor = torch.tensor(prot_seq_tokenized).type(torch.LongTensor)

            # the shapes are     {<int>:   ([prom_len +2],  integer,    [prot_len +2],   integer   )}                                                         )}
            prom_prot_dict[sample_index] = (prom_tensor, valid_prom_len, prot_tensor, valid_prot_len)
        
        # store the tensor info for later indexing
        return prom_prot_dict


def get_promoter_vocab():
    """ Returns a d2l Vocab class instance made for promoter tokens.

    returns <Vocab>: all possible tokens for a promoter including A C G T and padding, beginning-of-sentence and end-of-sentence
    """
    nucleotides = ["A", "C", "G", "T"]
    promoter_vocab = d2l.Vocab(tokens=nucleotides, min_freq=0, reserved_tokens=['<pad>', '<bos>', '<eos>'])
    return promoter_vocab


def get_protein_vocab(add_asterisk_to_vocab=False, use_selenocysteine=False):
    """  Returns a d2l Vocab class instance made for protein tokens.

    add_asterisk_to_vocab <bool>: include the asterisk as a possible token in the vocab.
    returns <Vocab>: all possible tokens for a protein including ARNDCQEGHILKMFPSTWYV padding,
                     beginning-of-sentence, end-of-sentence and possible the * symbol.
    """
    if add_asterisk_to_vocab:
        residues = [aa for aa in "ARNDCQEGHILKMFPSTWYV*"]
    else:
        residues = [aa for aa in "ARNDCQEGHILKMFPSTWYV"]
    if use_selenocysteine:
        residues.append("U")
    protein_vocab = d2l.Vocab(tokens=residues, min_freq=0, reserved_tokens=['<pad>', '<bos>', '<eos>'])
    return protein_vocab


def tokenize_sequence(input_seq, vocab, num_steps):
    """ Tokenize a sequence string into a list of token indexes.

    input_seq: <str> the sequence which must be encoded into tokens.
    vocab: <d2l Vocab> a vocab with all possible token indexes and token values for this sequence.\
    num_steps: <int> The length to which all input will be padded to.
    returns: <list> token indexes for each position in the sequence, with <bos> and <eos> tokens added.
    """
    # input sequence length + 2 for the <bos> token and <eos> token
    seq = [letter for letter in input_seq]
    seq = [vocab.token_to_idx[pos] for pos in seq]
    seq = [vocab.token_to_idx['<bos>']] + seq + [vocab.token_to_idx['<eos>']]
    seq = d2l.truncate_pad(seq, num_steps=num_steps, padding_token=vocab['<pad>'])

    return seq


def untokenize_sequence(tokenized_tensor, vocab):
    """ Change a list of token indexes back into a sequence string.

    tokenized_tensor: <list> or <1D torch tensor> a list of vocab indexes.
    vocab: <d2l Vocab> a vocab with all possible token indexes and token values for this sequence.
    returns: <str> the sequence translated back from the list of token indexes.
    """

    seq = ""
    for index in tokenized_tensor:
        token = vocab.idx_to_token[index]
        if token == '<pad>' or token == '<bos>':
            seq = f"{seq}"
        elif token == "<eos>":
            return seq
        else:
            seq = f"{seq}{token}"

    return seq


def parse_dataset(dataset_path):
    """
    dataset_path: path to the tab delimited dataset.
    returns: list of tuples for each sample with the format:
        [(<int> sample index, <str> 'Train' or 'Test', <str> promoter sequence, <str> protein sequence), ]
    """
    sample_list = list()
    with open(dataset_path, 'r') as dataset:
        for line in dataset.readlines():
            line = line.strip()
            if line.startswith("#"):
                continue
            sample_index, sample_type, prom_seq, prot_seq = line.split("\t")[0:4]
            sample_index = int(sample_index)
            if sample_type not in ("Train", "Test"):
                raise ValueError(f"Sample with index {sample_index} has an incorrect sample type.")

            sample_list.append((sample_index, sample_type, prom_seq, prot_seq))
    
    return sample_list


def load_dataset(dataset_path, src_vocab, tgt_vocab, batch_size):
    """ Get a train and test data iter which the LSTM-LSTM model can use during training.

    dataset_path: <str> the path to the dataset containing the promoter-protein pairs.
    src_vocab: <d2l vocab> vocabulary to translate promoter tokens to nucleotides.
                can be obtained through the get_promoter_vocab() function.
    tgt_vocab: <d2l vocab> vocabulary to translate protein tokens to amino acids.
                can be obtained through the get_protein_vocab() function.
    batch_size: <int> the number of promoter-protein samples to include per batch.

    returns: train iter and test iter containing promoter-protein batch tensors.
    """
    sample_list = parse_dataset(dataset_path)

    # Get train test indexes
    train_indexes = list()
    test_indexes = list()
    for sample_index, sample_type, prom_seq, prot_seq in sample_list:
        if sample_type == "Train":
            train_indexes.append(sample_index)
        elif sample_type == "Test":
            test_indexes.append(sample_index)
    # Check validity of train and indexes
    if len(set(train_indexes).intersection(set(test_indexes))) > 0:
        raise ValueError("Train and test set shares indexes (samples), the dataset is corrupt!")
    if len(train_indexes) < 1 or len(test_indexes) < 1:
        raise ValueError("The train or test set contains no indexes (samples), check dataset used as input!")
    if len(train_indexes) != len(set(train_indexes)) or len(test_indexes) != len(set(test_indexes)):
        raise ValueError("The train or test set has duplicate indexes, the dataset is corrupt!")

    # Get maximum promoter and protein lengths, used later for padding tensors
    largest_promoter_length = max([len(info[2]) for info in sample_list])
    largest_protein_length = max([len(info[3]) for info in sample_list])

    # Create dataset and split into (predetermined) train and test
    dataset = PromProtDataset(sample_list=sample_list, src_vocab=src_vocab, tgt_vocab=tgt_vocab,
                              largest_promoter_length=largest_promoter_length, largest_protein_length=largest_protein_length)

    train_dataset = torch.utils.data.Subset(dataset=dataset, indices=train_indexes)
    train_iter = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)
    test_dataset = torch.utils.data.Subset(dataset=dataset, indices=test_indexes)
    test_iter = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=batch_size, shuffle=True)
    return train_iter, test_iter


def amino_acid_counts(path_to_dataset):
    """ Prints the amino acid distribution of a dataset.

    path_to_dataset: <str> path to the dataset.
    """
    sample_list = parse_dataset(path_to_dataset)
    train_occurences = dict()
    test_occurences = dict()
    for sample_index, sample_type, prom_seq, prot_seq in sample_list:
        # count for train split of the dataset

        if sample_type == "Train":
            for amino_acid in prot_seq:
                if not amino_acid in train_occurences:
                    train_occurences[amino_acid] = 0
                train_occurences[amino_acid] += 1
        # count for test split of the dataset
        else:
            for amino_acid in prot_seq:
                if not amino_acid in test_occurences:
                    test_occurences[amino_acid] = 0
                test_occurences[amino_acid] += 1
    sorted_amino_acids = sorted(set(list(train_occurences.keys()) + list(test_occurences.keys())))
    header_string = ""
    value_string = ""
    for amino_acid in sorted_amino_acids:
        train_count = train_occurences[amino_acid]
        train_total = sum(train_occurences.values())
        test_count = test_occurences[amino_acid]
        test_total = sum(test_occurences.values())

        header_string = f"{header_string}'{amino_acid}', "
        value_string =  f"{value_string} {(train_count+test_count)/(train_total+test_total)}, "
        #print("Amino_acid TotalCountInDataset Percent")
        #print(f"{amino_acid} {train_count+test_count} {(train_count+test_count)/(train_total+test_total)}")
    print(header_string)
    print(value_string)


if __name__ == "__main__":
    """
    """
    amino_acid_counts(r"datasets\prom2prot\2022_03_22_08h_57m_01s__PPD_maxproteinlength_None__MaxPerIdt0.5.txt")
