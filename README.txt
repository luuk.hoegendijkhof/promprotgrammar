This git repository contains the code used in the MSc thesis "Learning prokaryote promoter-protein grammar via deep learning". Two deep learning architectures (LSTM-LSTM and CNN-LSTM) use promoters to predict protein sequences and protein domains respectively. The code is still being used and updated.


The Seq2Seq???.py files contain the code for the LSTM-LSTM (sequence-to-sequence) (promoter-to-protein) model.
The DeePromoter???.py files contain the code for the CNN-LSTM (domain classifier) (promkoter-to-protein domain) model.
The ParseLogfile.py function contains the functions to extract info from the logfiles of both types of models.
The PPD_data folder contains the raw PPD data.
    It also contains two ppd_parser.py files. One to generate promoter-protein datasets and one to generate promoter-domain datasets.
The Datasets folder contains the datasets used to train the LSTM-LSTM and the datasets used to train the CNN-LSTM.
The results folder contains all results and stored models for this thesis.
    It also contains R scripts to create the plots as seen in the thesis.

