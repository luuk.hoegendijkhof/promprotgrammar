"""
This file generates a promoter-protein sequence dataset from PromBase and
genbank files. Used to generate a medium gamma proteobacteria dataset used in
the Author's master's thesis. 

Takes a path to a .txt file as input. This .txt should be in space delimited format
with each line containing both the name of a PromBase allgeneinfo.txt file and the
name of the associated GenBank file (.gbff format).

Luuk Hoegen Dijkhof
WUR bioinformatics group
14 feb 2022
"""
from Bio import Align
import random
from os import path
from datetime import datetime
from ppd_parser_domain_dataset import get_file_pair_locations, get_protein_sequence_dict, get_matching_promoter_proteins


def generate_ppd_domain_dataset(target_alldata_file, genbank_folder = r"./ppd_genbank/",
                                ppd_folder = r"./ppd_csv/", ppd_info_path = r"./ppd_file_info.csv"):
    """ Generate a .txt file with all the findable promoter-protein pairs from the PPD dataset.

    target_alldata_file: <str> path to the target file.
    ppd_folder: <str> path to the folder with the PPD .csv files
    genbank_folder: <str> path to the folder with the genbank files.
    ppd_info_path: <str> path to the file with the matching genbank and .csv files.
    """
    if path.exists(target_alldata_file):
        print("WARNING: Target fasta already exists. Not creating a new one.")
        return

    # Go over the PPD .csv file and matching genkbank file for each organism
    matching_files = get_file_pair_locations(genbank_folder=genbank_folder, ppd_info_path=ppd_info_path, ppd_folder=ppd_folder)
    with open(target_alldata_file, 'a') as target_file:
        target_file.write("#assembly_identifier\tpromoter\tprotein\tspecies\n")
        for strain, promoter_file, genbank_file in matching_files:
            # get all proteins in the genbank file
            all_protein_dict = get_protein_sequence_dict(full_path_to_genbank_file=genbank_file)
            # get matching promoter-proteins
            prom_prot_list = get_matching_promoter_proteins(promoter_file=promoter_file, all_protein_dict=all_protein_dict,
                                                            minimum_promoter_length=0, max_distance_to_gene=99999)
            # for each promoter-protein pair found
            for gene_identifier_used, promoter_seq, protein_seq, utr_len in prom_prot_list:
                strain = strain.replace(' ', '_')
                #fasta_identifier = f'{strain}___{gene_identifier_used}___{promoter_seq}___{utr_len}'
                target_file.write(f"{genbank_file}___{gene_identifier_used}\t{promoter_seq}\t{protein_seq}\t{strain}\n")


def get_align_percentage(seq1, seq2):
    """ Calculate the align value between two sequences.

    seq1: <str>
    seq2: <str>
    returns: value between 0 and 1.
    """
    aligner = Align.PairwiseAligner()
    align_score = aligner.score(seq1, seq2)
    align_percentage = align_score / max(len(seq1), len(seq2))  
    return align_percentage


def homologs_in_train_data(current_protein, train_identifiers, all_prom_proteins,
                           max_percent_identity):
    """ Check if this protein has a homolog in the train data.

    current_protein: <str> current protein sequence.
    train_identifiers: <list of <str>> list of all gene identifiers in the train set.
    all_prom_proteins: <dict> with format {gene_identifier: (promoter <str>, protein <str>, species <str>)}
                        obtained with the get_all_prom_protein_pairs function.
    max_percent_identity: <float> value between 0 and 1.
                          how much percent identity is allowed. if higher than this value the
                          proteins are considered homologs. To prevent test set contamination
                          homologs between the train and test set are removed from the test set.

    returns: <bool> True if homolog found, False if no homolog found.
    """
    # check for homology (within train samples)
    for train_identifier in train_identifiers:
        # do not add this protein to the data if the sequence identity is too high
        train_protein_seq = all_prom_proteins[train_identifier][1]
        align_percentage = get_align_percentage(current_protein, train_protein_seq)

        if align_percentage > max_percent_identity:
            return True

    return False


def get_all_prom_protein_pairs(prom_prot_pair_file, minimum_protein_length):
    """ Read the file with all promoter-protein pairs and filter for minimum length.

    prom_prot_pair_file: <str> path to file with all promoter-protein pairs.
                         this file can be generated with the generate_ppd_domain_dataset function.
    minimum_protein_length: <int> proteins shorter than this are filtered out.

    returns: <dict> with information of all viable promoter-protein pairs with format:
             {gene_identifier: (promoter <str>, protein <str>, species <str>)}
    """
    all_prom_proteins = dict()
    with open(prom_prot_pair_file, 'r') as prom_prot_info_file:
        for line in prom_prot_info_file.readlines()[1:]:
            line = line.strip()
            if line:
                identifier, promoter, protein, species = line.split("\t")
                promoter = promoter.upper()

                if minimum_protein_length:
                    if len(protein) < minimum_protein_length:
                        continue

                # these amino acids are very rare and occur in ~15 proteins.
                # skip them so the amino acid frequency adjustment does not have to use these AAs.
                if "U" in protein or "X" in protein:
                    continue
                
                
                # store the protein info
                all_prom_proteins[identifier] = (promoter, protein, species)
    return all_prom_proteins


def get_dataset(prom_prot_pair_file, train_test_ratio, target_folder, max_percent_identity=None,
                max_protein_length=None, number_of_first_amino_acids=None,
                minimum_protein_length=None):
    """ Generate a promoter-protein dataset.txt the LSTM-LSTM model can train on.

    prom_prot_pair_file: <str> path to file with all promoter-protein pairs.
                         this file can be generated with the generate_ppd_domain_dataset function.
    train_test_ratio: <float> between 0 and 1. Ratio of train samples: test samples.
    target_folder: <str> the folder where the dataset will be created.
    max_percent_identity: <float> between 0 and 1. The max percent identity allowed.
                          Percent identity higher than this and proteins are considered homologs.
                          Homologs between the train and test set are filtered out the test set to prevent contamination.
    max_protein_length: <int> remove proteins longer than this length.
    number_of_first_amino_acids: <int> use the first ??? amino acids of proteins.
                                trim proteins to shorten them and speed up runtime.
                                None: if not used.
    minimum_protein_length: <int> filter out proteins shorter than this value.
    """
    # 0) Check folder
    if not path.exists(target_folder):
        raise ValueError("Target folder does not exist! Please create the target folder.")
    # 1) Log settings used
    runtime_start = datetime.now().strftime(r'%Y_%m_%d_%Hh_%Mm_%Ss')
    file_name = f"{target_folder}/{runtime_start}__PPD_maxproteinlength_{max_protein_length}__MaxPerIdt{max_percent_identity}.txt"
    with open(file_name, 'a') as dataset_file:
        dataset_file.write(f"#Train test ratio used: {train_test_ratio}\n")
        dataset_file.write(f"#Minimum promoter length used: 20\n")
        dataset_file.write(f"#Maximum allowed UTR length (distance promoter and gene): 200 nucleotides\n")
        dataset_file.write(f"#Maximum percent identity between test proteins and train proteins allowed: {max_percent_identity}\n")
        dataset_file.write(f"#Maximum length of proteins allowed: {max_protein_length}\n")
        dataset_file.write(f"#Used only the first {number_of_first_amino_acids} or less number of amino acids for each protein.\n")
        dataset_file.write(f"#Sample_index\tSample_type\tProm_seq\tProt_seq\tIdentifier\tSpecies\n")

    # 2) Get all prom prot pairs
    all_prom_proteins = get_all_prom_protein_pairs(prom_prot_pair_file=prom_prot_pair_file, minimum_protein_length=minimum_protein_length)

    train_identifiers = [identifier for identifier in all_prom_proteins.keys() if random.random() <= train_test_ratio]
    # 3) Write data to the file
    with open(file_name, 'a') as dataset_file:
        sample_index = -1
        
        for identifier, (prom_seq, protein_seq, species) in all_prom_proteins.items():
            sample_index += 1
            
            if identifier in train_identifiers:
                type = "Train"
            else:
                type = "Test"
                # check if test protein is homologous (within train samples)
                if max_percent_identity:
                    if homologs_in_train_data(current_protein=protein_seq, train_identifiers=train_identifiers,
                                            all_prom_proteins=all_prom_proteins, max_percent_identity=max_percent_identity):
                        print(sample_index, "found homolog")
                        continue
            print(sample_index, type)
            if number_of_first_amino_acids:
                if len(protein_seq) > number_of_first_amino_acids:
                    protein_seq = protein_seq[:number_of_first_amino_acids]

            if max_protein_length:
                if len(protein_seq) > max_protein_length:
                    continue

            dataset_file.write(f"{sample_index}\t{type}\t{prom_seq}\t{protein_seq}\t{identifier}\t{species}\n")


if __name__ == "__main__":
    # prom prot pair file obtained through PPD parser of domain classifier
    generate_ppd_domain_dataset(r"./AllData_seq2seq.txt")
    get_dataset(prom_prot_pair_file=r"./AllData_seq2seq.txt",
        train_test_ratio=0.75, max_percent_identity=0.5, target_folder="./", max_protein_length=None,
        number_of_first_amino_acids=100, minimum_protein_length = 80)
