"""
"""
from Bio import SeqIO
from Bio import SearchIO  

import csv
import os
import random


def get_file_pair_locations(ppd_info_path, genbank_folder, ppd_folder):
    """ Get matching promoter and protein file names for each unique species.\
    
    ppd_info_path: <str> path to the .csv file with the matching PPD and genbank file info.
    ppd_folder: <str> path to the folder with the PPD .csv files.
    genbank_folder: <str> path to the folder with the genbank files.
    returns: list of tuples with the format:
              (<str> species, <str> path to promoter_file, <str> path to protein_file)
    """
    # Get unique species
    with open(ppd_info_path, newline="\n") as csvfile:
        csv_reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        ppd_info = [row for row in csv_reader]
    unique_strain_names = set([item['Strain Name'] for item in ppd_info])

    matching_files = list()
    for strain in unique_strain_names:
        # get promoter file
        promoter_file = f"{ppd_folder}/{strain} promoters.csv"
        if not os.path.exists(promoter_file):
            print("WARNING file does not exist. Skipping!", promoter_file)
            continue

        # get assembly file
        assembly_code = [info['Assembly'] for info in ppd_info if info['Strain Name'] == strain][0]
        genbank_file = ""
        for possible_genbank_file in os.listdir(genbank_folder):
            if assembly_code in possible_genbank_file:
                if genbank_file:
                    raise ValueError(f"Duplicate genbank file {possible_genbank_file} found for {strain}")
                genbank_file = f"{genbank_folder}/{possible_genbank_file}"
        
        # check if strain is used
        if not genbank_file:
            print(f"WARNING! Strain not used: {strain}\tWith assembly code: {assembly_code}")
            continue
        
        matching_files.append((strain, promoter_file, genbank_file))
    return matching_files


def get_protein_sequence_dict(full_path_to_genbank_file):
    """ Get a dictionary with all protein names and protein translations in a genbank assembly file.

    full_path_to_genbank_file: <str> path to a genbank file.
    returns: <dict> {<str> gene identifier: <str> protein sequence}
    """
    protein_seq_dict = dict()
    for record in SeqIO.parse(full_path_to_genbank_file, "genbank"):
        for feature in record.features:
            if feature.type == "CDS":
                # get info for this CDS
                protein_translation = feature.qualifiers.get('translation')
                old_locus_tag = feature.qualifiers.get('old_locus_tag')
                locus_tag = feature.qualifiers.get('locus_tag')
                protein_id = feature.qualifiers.get('protein_id')
        
                # check if this CDS has a protein
                if not protein_translation:
                    continue

                identifiers = (old_locus_tag, locus_tag, protein_id)
                for identifier in identifiers:
                    if identifier:
                        protein_seq_dict[identifier[0]] = protein_translation[0]

    return protein_seq_dict


def get_matching_promoter_proteins(promoter_file, all_protein_dict, minimum_promoter_length, max_distance_to_gene):
    """ Get all matching promoter-protein pairs for 1 promoter file and 1 protein dict.
    
    promoter_file: <str> path to a PPD .csv file with promoter sequences.
    all_protein_dict: <dict> {<str> gene identifier: <str> protein sequence}
    minimum_promoter_length: <int> minimum promoter length. Filter out smaller promoters.
    max_distance_to_gene: <int> max distance between promoters and genes. Filter out if distance is larger.

    returns: <list> for each suitable prom-prot pair a tuple with:
             (gene_identifier_used, promoter_seq, protein_seq, utr_len))
    """
    prom_prot_list = list()
    genes_used = list()
    
    with open(promoter_file, newline="\n") as csvfile:
        csv_reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        for prom_dict in csv_reader:
            # get promoter
            promoter = prom_dict['PromoterSeq']
            if len(promoter) < minimum_promoter_length:
                continue
            
            # check UTR distance
            if prom_dict["TSSPosition"] == "NA":
                continue
            tss_pos = int(prom_dict["TSSPosition"])
            if prom_dict["GeneStart"] == "NA" and prom_dict["GeneEnd"] == "NA":
                continue
            if prom_dict["GeneStart"] != "NA":
                utr_len = abs(tss_pos - int(prom_dict["GeneStart"]))
            if prom_dict["GeneEnd"] != "NA":
                utr_len = min(utr_len, abs(tss_pos - int(prom_dict["GeneEnd"])))
            if utr_len > max_distance_to_gene:
                continue

            # get any possible protein
            if prom_dict["GeneName"] in all_protein_dict and prom_dict["GeneName"] != "NA":
                if prom_dict["GeneName"] in genes_used:
                    continue
                identifier_used = prom_dict["GeneName"]
                protein = all_protein_dict[identifier_used]
            elif prom_dict["Locus_tag"] in all_protein_dict and prom_dict["Locus_tag"] != "NA":
                if prom_dict["Locus_tag"] in genes_used:
                    continue
                identifier_used = prom_dict["Locus_tag"]
                protein = all_protein_dict[identifier_used]
            else:
                continue

            prom_prot_list.append((identifier_used, promoter, protein, utr_len))
            #print(prom_dict["GeneName"], prom_dict["Locus_tag"], promoter[0:30], protein[0:30])
            genes_used.append(prom_dict["GeneName"])
            genes_used.append(prom_dict["Locus_tag"])

    return prom_prot_list


def parse_hmmscan_hmmer3_text_file(path_to_hmmer3_text_file):
    """ Get all protein domains predicted by HMMScan per protein.

    returns: <dict> with format
            {<str> description from fasta file: <list> [<SearchIO Hit class>, ..] }
    """
    # settings
    hit_dict = dict()

    with open(path_to_hmmer3_text_file, 'r') as input_file:
        for qresult in SearchIO.parse(input_file, 'hmmer3-text'):
            description = qresult.id  #sequence ID from fasta
            hits = qresult.hits
            hit_dict[description.strip()] = hits

    return hit_dict


def get_range_overlap(start1, end1, start2, end2):
    """ The largest overlap ratio of two ranges
    
    This function is used to check two protein domain ranges don't overlap too much.
    """
    range_1 = range(start1, end1+1)
    range_2 = range(start2, end2+1)
    overlapping_positions = set(range_1).intersection(set(range_2))
    shortest_length = min(len(range_1), len(range_2))
    return len(overlapping_positions) / shortest_length


def filter_hsp_for_overlap(hsp_list, new_hsp, max_overlap):
    """ Given a list of domains (HSP) (high scoring pair), check if a new HSP overlaps significantly.

    hsp_list: list of <SearchIO Hit class> for each predicted domain.
    new_hsp: new <SearchIO Hit class> which may or may not overlap with known HSPs
    max_overlap: <float> value between 0 and 1.
                 How much a domain may overlap with another before it is removed (0 to 100%)
    
    returns: updated list of HSPs (high scoring pairs)
    """
    indexes_to_remove = list()
    # if there are already hits, check there is not significant overlap.
    for hsp_index, hsp in enumerate(hsp_list):
        overlap_with_new_hsp = get_range_overlap(start1=hsp.query_range[0],
                                                 end1=hsp.query_range[1],
                                                 start2=new_hsp.query_range[0],
                                                 end2=new_hsp.query_range[1])
        # if there is too much overlap
        if overlap_with_new_hsp > max_overlap:
            # if the new hsp has the bigger e value (is worse)
            if hsp.evalue > new_hsp.evalue:
                indexes_to_remove.append(hsp_index)
            else:
                # do not edit the hsp_list, as the new hsp is worse
                return hsp_list
    
    # remove indexes 
    filtered_list = [hsp for hsp_index, hsp in enumerate(hsp_list) if hsp_index not in indexes_to_remove]
    
    # the new hsp passed the criteria, so add it
    filtered_list.append(new_hsp)

    return filtered_list


def filter_domain_hits(hit_dict, max_e_value, max_overlap):
    """ Filter protein domain hits based on E value and max overlap between domains.

    hit_dict = <dict> with format
            {<str> description from fasta file: <list> [<SearchIO Hit class>, ..] }
    max_e_value = <float> max E value before a domain is filtered out.
                          The higher the E value the less confident the domain prediction is.
    max_overlap: <float> value between 0 and 1.
                How much a domain may overlap with another before it is removed (0 to 100%)

    returns: <dict> with format {'gene description': [<SearchIO Hit class>, ..]}
    """
    filtered_hit_dict = dict()
    # for each sequence, and its list of hits
    for fasta_desc, list_of_hits in hit_dict.items():
        valid_hsps = list()
        # for each hit: get valid hsps (high scoring pairs/domains)
        for hit in list_of_hits:
            # get best high scoring pair (hsp)
            best_high_scoring_pair = None
            lowest_e_value = 1
            for high_scoring_pair in list(hit.hsps):
                if high_scoring_pair.evalue < lowest_e_value:
                    best_high_scoring_pair = high_scoring_pair
                    lowest_e_value = high_scoring_pair.evalue
            # if there is no match, go to the next hit
            if not best_high_scoring_pair:
                continue
            # if the E-value of this domain is too high, continue to next hit
            if best_high_scoring_pair.evalue > max_e_value:
                continue

            if not valid_hsps:
                # if there are no valid hits yet, add this one
                valid_hsps.append(best_high_scoring_pair)
            else:
                # see if there is significant overlap between hsps, act accordingly
                valid_hsps = filter_hsp_for_overlap(hsp_list=valid_hsps, new_hsp=best_high_scoring_pair,
                                                    max_overlap=max_overlap)
        if valid_hsps:
            filtered_hit_dict[fasta_desc] = valid_hsps

    return filtered_hit_dict


def generate_ppd_domain_dataset():
    """ Function used to generate the PPD domain dataset and intermediate files.

    Contains an intermediate step in which the user has to run HMMScan to predict the protein domains.
    """
    # 0) settings
    genbank_folder = r"./ppd_genbank/"
    ppd_folder = r"./ppd_csv/"
    ppd_info_path = r"./ppd_file_info.csv"
    minimum_prom_len = 20
    max_distance_to_gene = 200
    minimum_domain_occurence = 25
    max_e_value = 0.0001
    max_overlap = 0.5
    train_ratio = 0.8

    target_fasta_file = r"./ppd_intermediate_fasta_for_SMART.txt"
    target_alldata_path = f"./AllData_domains_{minimum_prom_len}_maxutrlen{max_distance_to_gene}_max_evalue{max_e_value}_maxoverlap{max_overlap}.txt"
    target_dataset_path = f"./ppd_dataset_minpromlen{minimum_prom_len}_maxutrlen{max_distance_to_gene}_max_evalue{max_e_value}_maxoverlap{max_overlap}_minoccurence{minimum_domain_occurence}.txt"
    matching_files = get_file_pair_locations(genbank_folder=genbank_folder, ppd_info_path=ppd_info_path, ppd_folder=ppd_folder)
    
    # 1) create fasta file with each prom-prot pair (if it does not exist already)
    write_to_fasta_file = True
    if os.path.exists(target_fasta_file):
        print("WARNING: Target fasta already exists. Not creating a new one.")
        write_to_fasta_file = False

    with open(target_fasta_file, 'a') as fasta_file:
        all_prom_prot_dict = dict()
        for strain, promoter_file, genbank_file in matching_files:
            all_protein_dict = get_protein_sequence_dict(full_path_to_genbank_file=genbank_file)
            prom_prot_list = get_matching_promoter_proteins(promoter_file=promoter_file, all_protein_dict=all_protein_dict,
                                                            minimum_promoter_length=minimum_prom_len, max_distance_to_gene=max_distance_to_gene)
            
            for gene_identifier_used, promoter_seq, protein_seq, utr_len in prom_prot_list:
                strain = strain.replace(' ', '_')
                fasta_identifier = f'{strain}___{gene_identifier_used}___{promoter_seq}___{utr_len}'
 
                if write_to_fasta_file:
                    fasta_file.write(f'>{fasta_identifier}\n')
                    fasta_file.write(f'{protein_seq}\n')
                all_prom_prot_dict[fasta_identifier] = protein_seq
            

    # 2) BY USER, run hmmer to get domains from the protein fasta file
    # example: hmmscan  -o predicted_domains.txt  Pfam-A.hmm ppd_intermediate_fasta_for_SMART.txt
    path_to_hmmer3_text_file = input('Run hmmer3 on linux with the intermediate fasta file against the PFAM A domain database ' \
                                     + ', give path to the output file (with hmmer3-text format): ')
    path_to_hmmer3_text_file = path_to_hmmer3_text_file.strip().replace('"', '').replace("'", '')

    # 3) Parse the hmmer file for domains.
    hit_dict = parse_hmmscan_hmmer3_text_file(path_to_hmmer3_text_file)
    print("Hits without filtering for overlap and e-value.", len(hit_dict))
    # 3b) Remove hits with too much overlap or high e value
    hit_dict = filter_domain_hits(hit_dict=hit_dict, max_e_value=max_e_value, max_overlap=max_overlap)
    print("Hits after filtering", len(hit_dict))

    # 4) Create the alldata file (if it does not exist already)
    if os.path.exists(target_alldata_path):
        print(f"WARNING: File {target_alldata_path} with all the raw data already exists! Not creating a new one.")
    else:
        with open(target_alldata_path, 'a') as target_alldata_file:
            target_alldata_file.write("Prom_seq\tProt_seq\tDomains\tStrain\tGene_identifier\tUTR_len\n")
            # go over each promoter-protein pair
            for fasta_identier, protein in all_prom_prot_dict.items():
                hit_list = hit_dict.get(fasta_identier.strip())
                strain, gene_identifier, promoter_seq, utr_len = fasta_identier.split("___")
                # if the protein has hits (domains), write to dataset file.
                if hit_list:
                    unique_ids = set([hit.hit_id for hit in hit_list])
                    target_alldata_file.write(f"{promoter_seq}\t{protein}\t{' '.join(unique_ids)}\t" + \
                                    f"{strain}\t{gene_identifier}\t{utr_len}\n")
    
    # 5) get domain statistics
    domain_occurence = dict()
    with open(target_alldata_path, 'r') as alldata_file:
        for line_index, line in enumerate(alldata_file.readlines()):
            line = line.strip()
            # skip any possible empty lines
            if not line:
                continue

            split_info = line.split("\t")
            # get indexes of data from header line 
            if line_index == 0:
                domain_index = split_info.index('Domains')
                continue

            domains = split_info[domain_index].split(" ")
            for domain in domains:
                if domain not in domain_occurence:
                    domain_occurence[domain] = 0
                domain_occurence[domain] += 1

    # 6) Create dataset
    # Open target dataset file to write to
    with open(target_dataset_path, 'a') as target_dataset_file:
        with open(target_alldata_path, 'r') as target_alldata_file:
            sample_index = -1
            for line_index, line in enumerate(target_alldata_file.readlines()):
                line = line.strip()
                if not line:
                    continue
                split_info = line.split("\t")
                # get indexes from header line
                if line_index == 0:
                    domain_index = split_info.index('Domains')
                    prom_index = split_info.index('Prom_seq')
                    prot_index = split_info.index("Prot_seq")
                    identifier_index = split_info.index("Gene_identifier")
                    strain_index = split_info.index("Strain")
                    target_dataset_file.write(f"#Sample_index\tSample_type\tProm_seq\tDomains\tProt_seq\tIdentifier\tSpecies\n")			
                    continue

                domains = split_info[domain_index]
                prom = split_info[prom_index]
                prot = split_info[prot_index]
                identifier = split_info[identifier_index]
                strain = split_info[strain_index]
                valid_domains = list()
                for domain in domains.split(" "):
                    if domain_occurence[domain] >= minimum_domain_occurence:
                        valid_domains.append(domain)

                # If the prom-domain pair meets the requirements
                if valid_domains:
                    if random.random() <= train_ratio:
                        sample_type = "Train"
                    else:
                        sample_type = "Test"

                    sample_index += 1
                    target_dataset_file.write(f"{sample_index}\t{sample_type}\t{prom}\t{' '.join(valid_domains)}\t" + \
                                              f"{prot}\t{identifier}\t{strain}\n")


if __name__ == "__main__":
    # function used to generate the PPD promoter-domain dataset
    generate_ppd_domain_dataset()
