""" This file contains the sequence-to-sequence model architecture class used for
the LSTM-LSTM models. It also includes a function to get an instance of this class.

The code for this model was based on this repository:
https://github.com/bentrevett/pytorch-seq2seq/blob/master/1%20-%20Sequence%20to%20Sequence%20Learning%20with%20Neural%20Networks.ipynb
"""
import torch
import torch.nn as nn
import random

from Seq2Seq_LoadData import get_promoter_vocab, get_protein_vocab, untokenize_sequence, load_dataset


class Encoder(nn.Module):
    """ The encoder part of the model which extracts promoter information.
    """
    def __init__(self, input_dim, emb_dim, hid_dim, n_layers, dropout):
        """
        input_dim: number of input (promoter DNA) tokens
        emb_dim: the embedding size to use in the embedding layer.
        hid_dim: the number of hidden states of the LSTM.
        n_layers: the number of layers of the LSTM
        dropout: the chance for a neuron to 'drop out' (not be used) during training.
        """
        super().__init__()
        
        self.hid_dim = hid_dim
        self.n_layers = n_layers
        
        self.embedding = nn.Embedding(input_dim, emb_dim)
        self.rnn = nn.LSTM(emb_dim, hid_dim, n_layers, dropout = dropout)
        self.dropout = nn.Dropout(dropout)
        
    def forward(self, src):
        #src = [src len, batch size]  
        embedded = self.dropout(self.embedding(src))
        #embedded = [src len, batch size, emb dim]
        outputs, (hidden, cell) = self.rnn(embedded)
        #outputs = [src len, batch size, hid dim * n directions]
        #hidden = [n layers * n directions, batch size, hid dim]
        #cell = [n layers * n directions, batch size, hid dim]
        
        return hidden, cell


class Decoder(nn.Module):
    """ The decoder part of the model which generates protein sequences.
    """
    def __init__(self, output_dim, emb_dim, hid_dim, n_layers, dropout):
        """
        output_dim: number of output (protein amino acid) tokens
        emb_dim: the embedding size to use in the embedding layer.
        hid_dim: the number of hidden states of the LSTM.
        n_layers: the number of layers of the LSTM
        dropout: the chance for a neuron to 'drop out' (not be used) during training.
        """

        super().__init__()
        
        self.output_dim = output_dim
        self.hid_dim = hid_dim
        self.n_layers = n_layers
        self.embedding = nn.Embedding(output_dim, emb_dim)
        self.rnn = nn.LSTM(emb_dim, hid_dim, n_layers, dropout = dropout)
        self.fc_out = nn.Linear(hid_dim, output_dim)
        self.softmax = torch.nn.Softmax(dim=1)
        self.dropout = nn.Dropout(dropout)


    def forward(self, input, hidden, cell):
        
        #input = [batch size]
        #hidden = [n layers * n directions, batch size, hid dim]
        #cell = [n layers * n directions, batch size, hid dim]
        
        #n directions in the decoder will both always be 1, therefore:
        #hidden = [n layers, batch size, hid dim]
        #context = [n layers, batch size, hid dim]
        #print('decoder input', input.shape)
        input = input.unsqueeze(0)

        #input = [1, batch size]
        embedded = self.dropout(self.embedding(input.argmax(dim=2)))
        #embedded = [1, batch size, emb dim]      
        #print('embedded', embedded.shape)
        
        
        output, (hidden, cell) = self.rnn(embedded, (hidden, cell))

        #output = [seq len, batch size, hid dim * n directions]
        #hidden = [n layers * n directions, batch size, hid dim]
        #cell = [n layers * n directions, batch size, hid dim]
        
        #seq len and n directions will always be 1 in the decoder, therefore:
        #output = [1, batch size, hid dim]
        #hidden = [n layers, batch size, hid dim]
        #cell = [n layers, batch size, hid dim]
        
        prediction = self.fc_out(output.squeeze(0))
        prediction = self.softmax(prediction)

        #prediction = [batch size, output dim]
        return prediction, hidden, cell


class Seq2Seq(nn.Module):
    """ The full seq2seq model which combines the Encoder and Decoder classes.
    """
    def __init__(self, encoder, decoder, device):
        super().__init__()
        
        self.encoder = encoder
        self.decoder = decoder
        self.device = device
        
        assert encoder.hid_dim == decoder.hid_dim, \
            "Hidden dimensions of encoder and decoder must be equal!"
        assert encoder.n_layers == decoder.n_layers, \
            "Encoder and decoder must have equal number of layers!"


    def forward(self, src, trg, teacher_forcing_ratio=0.0, bos_token=2):
        """ Predict a protein sequence using a promoter input.

        src: input promoter with shape: [longest promoter length, batch size, number of nucleotide tokens]
        trg: target protein tensor (used for teacher forcing) with shape : 
        teacher_forcing_ratio: the chance ground truth is used instead of the previous prediction during training.
        bos_token: the number/token associated with the beginning of sequence token.

        returns: pytorch tensor with shape: [longest protein length, batch size, number of amino acid tokens]
                where the predicted sequence can be found using argmax of the amino acid probabilities.
        """
        batch_size = trg.shape[1]
        trg_len = trg.shape[0]
        trg_vocab_size = self.decoder.output_dim
        
        trg = torch.nn.functional.one_hot(trg, num_classes=trg_vocab_size).float()

        #last hidden state of the encoder is used as the initial hidden state of the decoder
        hidden, cell = self.encoder(src)

        #first input to the decoder is the <sos> tokens
        input = trg[0]

        #tensor to store decoder outputs
        output_storage = torch.zeros(trg_len, batch_size, trg_vocab_size).to(self.device)
        # the first output should always be a start of sequence token, give this token a probability of 1.0
        output_storage[0][:, torch.tensor(bos_token, dtype=torch.int)] = 1.0 

        for t in range(1, trg_len):
            #insert input token embedding, previous hidden and previous cell states
            #receive output tensor (predictions) and new hidden and cell states
            output, hidden, cell = self.decoder(input, hidden, cell)
 
            #place predictions in a tensor holding predictions for each token
            output_storage[t] = output
            #decide if we are going to use teacher forcing or not
            teacher_force = random.random() < teacher_forcing_ratio
            #get the highest predicted token from our predictions
            #if teacher forcing, use actual next token as next input
            #if not, use predicted token
            input = trg[t] if teacher_force else output
 
        return output_storage


def get_LSTM_LSTM_model(device='cpu'):
    """ Function to get a Seq2seq class instance.
    This model can be trained to learn and predict protein sequences based on promoter input.
    """
    src_vocab = get_promoter_vocab()
    tgt_vocab = get_protein_vocab(add_asterisk_to_vocab=True, use_selenocysteine=False)
    INPUT_DIM = len(src_vocab)
    OUTPUT_DIM = len(tgt_vocab)
    ENC_EMB_DIM = 250
    DEC_EMB_DIM = 250
    HID_DIM = 500
    N_LAYERS = 2
    ENC_DROPOUT = 0.2  # dropout not specified in article?
    DEC_DROPOUT = 0.2  # dropout not specified in article?
    enc = Encoder(INPUT_DIM, ENC_EMB_DIM, HID_DIM, N_LAYERS, ENC_DROPOUT)
    dec = Decoder(OUTPUT_DIM, DEC_EMB_DIM, HID_DIM, N_LAYERS, DEC_DROPOUT)
    model = Seq2Seq(enc, dec, device).to(device)

    return model


def show_model_predictions(dataset_path, trained_model_statedict_path, src_vocab, tgt_vocab, show_nr_of_predictions=10):
    """ This function shows a few example predictions of the trained model.

    dataset_path: <str> path with the dataset containing the input promoters.
    trained_model_statedict_path: <str> path to the trained model weights saved as a state dict .pt file
    src_vocab: <d2l vocab> vocabulary to translate promoter tokens to nucleotides.
                can be obtained through the get_promoter_vocab() function.
    tgt_vocab: <d2l vocab> vocabulary to translate protein tokens to amino acids.
                can be obtained through the get_protein_vocab() function.
    show_nr_of_predictions: <int> how many example predictions to show.
    """
    # set a low max num of threads so my personal PC doesn't explode
    max_num_of_threads = 2
    torch.set_num_threads(max_num_of_threads)

    # show ?? predictions at a time
    batch_size = show_nr_of_predictions

    # load model and data
    train_iter, test_iter = load_dataset(dataset_path=dataset_path,
                            src_vocab=src_vocab, tgt_vocab=tgt_vocab, batch_size=batch_size)
    model = get_LSTM_LSTM_model()
    model.load_state_dict(torch.load(trained_model_statedict_path))
    model.eval()

    # make predictions
    for batch in test_iter:
        prom_DNA, prom_len, prot_seq, prot_len = [x for x in batch]
        prom_DNA = prom_DNA.permute(1, 0)
        prot_seq = prot_seq.permute(1, 0)

        src = prom_DNA
        trg = prot_seq
        output = model(src, trg, teacher_forcing_ratio=0.5)
        print(output.shape)
        for seq_index, predicted_seq_tokens in enumerate(output.argmax(dim=2).permute(1, 0)):

            predicted_seq = untokenize_sequence(predicted_seq_tokens, vocab=tgt_vocab)
            correct_seq = untokenize_sequence(prot_seq.permute(1,0)[seq_index][1:], vocab=tgt_vocab)
            #print(f'{predicted_seq[:]}\t{correct_seq[:]}')
            print(f'{predicted_seq[:35]}\t{correct_seq[:35]}')
        exit()


if __name__ == "__main__":
    """
    """
    show_model_predictions(trained_model_statedict_path=r"results\output_and_saved_models\result_PPD_seq2seq_forced_diversity\2022_04_26_00h_32m_14s_Epoch99.pt",
                           show_nr_of_predictions = 32,
                           dataset_path=r"C:\Users\Coolmaster\Desktop\Code_files_thesis\final\datasets\prom2prot\2022_03_22_08h_57m_01s__PPD_maxproteinlength_None__MaxPerIdt0.5.txt",
                           src_vocab=get_promoter_vocab(), tgt_vocab=get_protein_vocab(add_asterisk_to_vocab=True, use_selenocysteine=True))
