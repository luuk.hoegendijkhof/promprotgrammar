""" This file contains the functions used to train and save CNN-LSTM models
which predict protein domains based on promoter input
"""
from DeePromoter import DeePromoter
from DeePromoterLoadData import load_dataset, get_total_domains_in_dataset
import torch
from datetime import datetime


def evaluate_network(net, dataset, device, loss_func, positive_weight_factor):
    """ Get average loss, accuracy and sensitivity of the model.

    net: the CNN-LSTM model (being trained)
    dataset: the (test) data iter to evaluate on.
    device: the device the tensors are stored on ('cpu')
    loss_func: the loss function class used to calculate the error/loss
    positive_weight_factor: <int> the compensation factor for present domains (1 instead of 0)

    returns:
        tensor of loss, float of average accuracy, float of domain sensitivity
    """
    net.eval()

    # set empty variables to store statistics of all batches
    loss_total = 0
    total_accuracy = 0
    total_domain_sensitivity = 0

    # evaluate batch by batch (to not overflow memory)
    for batch_nr, batch in enumerate(dataset):
        # get data for batch
        X, Y = [ii.to(device) for ii in batch]

        # get loss for this batch
        Y_hat = net(X)
        loss = loss_func(Y_hat, Y, positive_weight_factor)
        loss_total = loss.sum() + loss_total

        #  get total accuracy
        Y_hat = Y_hat.round().int()
        matches = torch.numel(Y_hat[Y_hat == Y])    # the length of matches = the number of matches
        accuracy = matches / torch.numel(Y)
        total_accuracy += accuracy

        # get positive domain accuracy
        domain_true_positives = torch.sum(Y_hat[Y_hat == Y]) 
        domain_sensitivity = domain_true_positives / (torch.sum(Y))
        total_domain_sensitivity += domain_sensitivity
    	
    # average out loss
    return loss_total/(batch_nr+1), total_accuracy/(batch_nr+1), total_domain_sensitivity/(batch_nr+1)


def train_domain_classifier(prom_len, dataset_path, kernel_sizes, positive_weight_factor, lr, nr_of_epochs,
                        batch_size=20):
    """ Upon calling this function the CNN-LSTM is trained to better predict promoter-domain relations.
    The performance over the epochs is stored in a logfile in the output folder. The final model
    is also saved in the output folder.

    prom_len: <int> length of the longest promoter input sequence.
    dataset_path: <str> path to the promoter-domain dataset.
    kernel_sizes = <list of integers> the 1D convolutional filter sizes used by the model.
                    the original DeePromoter article used [27, 14, 7]
    positive_weight_factor: <int> how much to compensate positive domains (1 instead of 0)
                            this compensation prevents the model from only predicting negative (0)
                            domains as these vastly outnumber present domains (1)
    lr: <float> the learning rate
    nr_of_epochs: number of epochs. How many times the model will pass through the full training set.
    batch_size: <int> the batch size used in both the training and test data iters.
    """
    # settings
    number_of_tokens =  5 # number of promoter tokens (at least 5)
    total_domains = get_total_domains_in_dataset(dataset_path)
    dropout=0.4
    net = DeePromoter(total_domains=total_domains, para_ker=kernel_sizes, input_shape=(batch_size, prom_len, number_of_tokens),
                     drop=dropout)
    net.apply(init_weights)
    train_iter, test_iter = load_dataset(dataset_path, batch_size, max_prom_len=None)
    criterion = get_weighted_BCELoss
    optimizer = torch.optim.Adam(net.parameters(), lr=lr)

    # log file
    now = datetime.now()
    time_string = now.strftime(r'%Y_%m_%d_%Hh_%Mm_%Ss')
    log_file_path = f"./output/{time_string}_logfile_DomainClassifier.txt"
    
    header_string = "Epoch Train_loss Train_acc Train_Domain_sens Test_loss Test_acc, Test_domain_sens"
    add_to_log(logfile_path=log_file_path, message=header_string, is_a_comment=True)
    model_stats = f"batch_size:{batch_size}\tnumber_of_domains:{total_domains}\tdataset:{dataset_path}\tdropout:{dropout}\tlr:{lr}"
    add_to_log(logfile_path=log_file_path, message=model_stats, is_a_comment=True)
    
    # train loop
    print("Epoch Train_loss Train_acc Train_Domain_sens Test_loss Test_acc, Test_domain_sens")
    for epoch in range(nr_of_epochs):
        net.train()
        
        for X, Y in train_iter:
            Y_pred = net(X)
            weighted_loss = criterion(Y_pred=Y_pred, Y=Y, positive_weight_factor=positive_weight_factor)
            weighted_loss.sum().backward()
            optimizer.step()

        if epoch % 25 == 0:
            with torch.no_grad():
                train_loss, train_acc, train_domain_sensitivity = evaluate_network(net=net, dataset=train_iter, device="cpu", loss_func=criterion, positive_weight_factor=positive_weight_factor)
                test_loss, test_acc, test_domain_sensitivity = evaluate_network(net=net, dataset=test_iter, device="cpu", loss_func=criterion, positive_weight_factor=positive_weight_factor)
                print(f"{epoch}\t{round(float(train_loss.item()), 4)}\t{round(train_acc, 4)}\t{round(float(train_domain_sensitivity), 4)}\t" + \
                    f"{round(float(test_loss.item()), 4)}\t{round(test_acc, 4)}\t{round(float(test_domain_sensitivity), 4)}")
                
                message = f"Epoch {epoch}\tTrain_loss {train_loss}\tTrain_acc {train_acc}\t" + \
                f"Train_sens {train_domain_sensitivity}\t" + \
                f"Test_loss {test_loss}\tTest_acc {test_acc}\t" + \
                f"Test_sens {test_domain_sensitivity}"
                add_to_log(logfile_path=log_file_path, message=message, is_a_comment=False)

    
    torch.save(net, f"./output/{time_string}_.pth")


def get_weighted_BCELoss(Y_pred, Y, positive_weight_factor):
    """ Custom version of the BCELoss which uses a positive weight factor to
        compensate the underrepresentation of present (1) domains compared to negative (0)
        domains.

    Y_pred: model prediction. torch float tensor with shape <number of possible domains>
    Y: ground truth. torch float tensor with shape <number of possible domains>
    positive_weight_factor: <int> how much to compensate positive domains (1 instead of 0)
                            this compensation prevents the model from only predicting negative (0)
                            domains as these vastly outnumber present domains (1)
    
    returns: mean weighted BCELoss of the prediction
    """
    criterion_raw = torch.nn.BCELoss(reduction='none')
    loss_raw = criterion_raw(Y_pred, Y)
    weight = torch.ones_like(loss_raw)
    weight[Y==1.] = positive_weight_factor
    weighted_loss = (loss_raw * weight).mean()
    return weighted_loss


def init_weights(model):
    """ Function to initiate model weights randomly.
    """
    if isinstance(model, torch.nn.Linear):
        torch.nn.init.xavier_uniform(model.weight)
        model.bias.data.fill_(0.01)


def add_to_log(logfile_path, message, is_a_comment=False):
    """ Add a message to the logfile.

    logfile_path: path to the logfile currently used during training.
    message: <str> the message to append to the logfile.
    is_a_comment: <bool> should be True if the comment does not describe epoch performance.
                  comments start with an # and are ignored when parsing logfiles.
    """
    now = datetime.now()
    full_message = f"{now.strftime(r'%Y_%m_%d_%Hh_%Mm_%Ss')}\t\t\t{message}\n"
    with open(logfile_path, 'a+') as f:
        if is_a_comment:
            f.write(f"#{full_message}")
        else:
            f.write(full_message)


def results_generate_artificial():
    """ Function used to train the CNN-LSTM on the artificial data.
        Data generated by this function was included in the report.
    """
    for repeat in range(10):
        train_domain_classifier(prom_len=50, positive_weight_factor=2, kernel_sizes=[17, 11, 5], lr=0.00005, nr_of_epochs=501,
                            dataset_path=r'datasets\prom2domain\ARTIFICIAL_patternlen6_promoterlen50_number_of_patterns5.txt')


def results_generate_real_data():
    """ Function used to train the CNN-LSTM on the biological PPD data.
        Data generated by this function was included in the report.
    """
    WEIGHT_FACTOR = 50
    LR = 0.00005
    for KERNEL_SIZE in ([27, 14, 7], [17, 11, 5]):
        for repeat in range(5):
            train_domain_classifier(prom_len=81, positive_weight_factor=WEIGHT_FACTOR, kernel_sizes=KERNEL_SIZE,
                                    lr=LR, nr_of_epochs=1001,
                                    dataset_path=r'datasets\prom2domain\ppd_dataset_minpromlen20_maxutrlen200_max_evalue0.0001_maxoverlap0.5_minoccurence25.txt')


if __name__ == "__main__":
    """
    """
    results_generate_artificial()
    #results_generate_real_data()
