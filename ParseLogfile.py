""" This file is used to extract information from the logfiles generated throughout
the training of a model. It is used to create accuracy plots (with R). It also includes
functions to generate sequence logos using saved models.
"""
from os import path as os_path
import ast
import matplotlib.pyplot as plt
import Seq2Seq_LoadData as load_data
import torch
import csv
from Seq2Seq_Model import get_LSTM_LSTM_model


def read_logfile(path_to_logfile):
    """ Extract all epoch info from a logfile

    path_to_logfile: <str>
    return: list with for each line in the logfile a dict with all info for that epoch.
            each epoch dict has format:
            {"Train_loss":<float>, "Test_loss":<float>, "Base_loss":<float>,
                        "Train_acc":<float>,  "Test_acc":<float>,  "Base_acc":<float>,
                          "Train_acc_over_len":<list of accuracies>, "Test_acc_over_len":<list of accuracies>,
                          "Base_acc_over_len":<list of accuracies>,
                          "Epoch": <int>, "Test_entropy":<float>, "Test_sens":<float>}
    """
    if not os_path.exists(path_to_logfile):
        raise ValueError("File specified does not exist!", path_to_logfile)
    
    logfile_information = list()
    with open(path_to_logfile, 'r') as logfile:
        # read each line
        for line in logfile.readlines():
            line = line.strip()
            if line.startswith("#"):
                continue
            # create dict to store info for one line
            epoch_dict = {"Train_loss":None, "Test_loss":None, "Base_loss":None,
                          "Train_acc":None,  "Test_acc":None,  "Base_acc":None,
                          "Train_acc_over_len":None, "Test_acc_over_len":None, "Base_acc_over_len":None,
                          "Epoch": None, "Test_entropy":None, "Test_sens":None}

            runtime, message = line.split("\t\t\t")
        
            # get separate parts for this epoch
            all_info = message.split("\t")
   
            for info in all_info:
                header, value = info.split(" ", 1)
                if header in ("Train_loss", "Test_loss", "Base_loss", "Train_acc", "Test_acc", "Base_acc", "Test_entropy", "Test_sens"):
                    epoch_dict[header] = float(value)
                elif header in ("Train_acc_over_len", "Base_acc_over_len", "Test_acc_over_len"):
                    epoch_dict[header] = ast.literal_eval(value)
                elif header == "Epoch":
                    epoch_dict[header] = int(value)
            # store the dict with info for this line
            logfile_information.append(epoch_dict)

    return logfile_information


def plot_graph(statistics, loss_plot, accuracy_plot, train_over_len_plot, test_over_len_plot):
    """ Old and messy function.
        Used to quickly plot model performance accuries (over sequence length) during training.

    statistics: dict with format {'epoch_nr':[], 'train_loss':[], 'train_acc':[], 'test_loss':[], 'test_acc':[], 'token_per_sec':[],  'baseline_loss':[], 'baseline_acc':[]} 
                this dict can be obtained using the 'read_logfile' function above.
    loss_plot: the matplotlib loss plot object.
    accuracy_plot: the matplotlib accuracy plot object.
    train_over_len_plot: the matplotlib object showing the train accuracy over length.
    test_over_len_plot: the matplotlib object showing the test accuarcy over length.
    """
    epochs = statistics['epoch_nr']

    # plot loss
    train_loss_y = statistics['train_loss']
    test_loss_y = statistics['test_loss']
    baseline_loss = statistics['baseline_loss']
    loss_plot.plot(epochs, train_loss_y, "tab:red", label="Train loss")
    loss_plot.plot(epochs, test_loss_y, "tab:blue", label="Test loss")
    loss_plot.plot(epochs, baseline_loss, "tab:gray", label="Baseline loss")
    # add a legend if it does not yet exist yet
    if not loss_plot.get_legend():
        loss_plot.legend(loc="upper right")
    
    # plot accuracy
    train_acc_plot_y = statistics['train_acc']
    test_acc_plot_y = statistics['test_acc']
    baseline_acc_plot_y = statistics['baseline_acc']
    accuracy_plot.plot(epochs, train_acc_plot_y, "tab:red", label='Train acc')
    accuracy_plot.plot(epochs, test_acc_plot_y, "tab:blue", label='Test acc')
    accuracy_plot.plot(epochs, baseline_acc_plot_y, "tab:gray", label='Baseline acc')
    # add a legend if it does not yet exist yet
    if not accuracy_plot.get_legend():
        accuracy_plot.legend(loc="upper left")
   
    # TRAIN plot accuracy over length
    train_acc_over_len = statistics['train_acc_over_len'][-1]
    train_acc_over_len_correct = [correct_incorrect[0] for correct_incorrect in train_acc_over_len]
    train_acc_over_len_incorrect = [correct_incorrect[1] for correct_incorrect in train_acc_over_len]
    train_acc_over_len_ratio = list()
    for correct, incorrect in train_acc_over_len:
        if correct == 0:
            train_acc_over_len_ratio.append(0.0)
        else:
            train_acc_over_len_ratio.append(correct / (correct+incorrect))
    train_over_len_plot.cla() # as the plot is reset each epoch, the labels must be updated again
    train_over_len_plot.set_title("Train Tokens predicted over length"); train_over_len_plot.set_xlabel('Seq index') # train_over_len_plot.set_ylim([0, 1.001]); 
    train_over_len_plot.bar([pos for pos in range(len(train_acc_over_len_correct))], train_acc_over_len_correct, label='correct', color="green")
    train_over_len_plot.bar([pos for pos in range(len(train_acc_over_len_incorrect))], train_acc_over_len_incorrect, bottom=train_acc_over_len_correct,  label="incorrect", color="red")
    # add accuracy ratio plot
    train_acc_axes2 = train_over_len_plot.twinx()
    train_acc_axes2.plot([pos for pos in range(len(train_acc_over_len_correct))], [ratio for ratio in train_acc_over_len_ratio], color='k', label='Accuracy ratio')
    train_acc_axes2.set_ylabel('Accuracy ratio')
    # add a legend if it does not yet exist yet
    if not train_over_len_plot.get_legend():
        train_over_len_plot.legend(loc="upper right")

    # TEST plot accuracy over length
    test_acc_over_len = statistics['test_acc_over_len'][-1]
    test_acc_over_len_correct = [correct_incorrect[0] for correct_incorrect in test_acc_over_len]
    test_acc_over_len_incorrect = [correct_incorrect[1] for correct_incorrect in test_acc_over_len]

    test_over_len_plot.set_title("Test Tokens predicted over length"); test_over_len_plot.set_xlabel('Seq index')
    test_over_len_plot.bar([pos for pos in range(len(test_acc_over_len_correct))], test_acc_over_len_correct, label='correct', color="green")
    test_over_len_plot.bar([pos for pos in range(len(test_acc_over_len_incorrect))], test_acc_over_len_incorrect, bottom=test_acc_over_len_correct,  label="incorrect", color="red")
    test_acc_over_len_ratio = list()
    for correct, incorrect in test_acc_over_len:
        if correct == 0:
            test_acc_over_len_ratio.append(0.0)
        else:
            test_acc_over_len_ratio.append(correct / (correct+incorrect))
    test_acc_axes2 = test_over_len_plot.twinx()
    test_acc_axes2.plot([pos for pos in range(len(test_acc_over_len_ratio))], [ratio for ratio in test_acc_over_len_ratio], color='k', label='Accuracy ratio')
    test_acc_axes2.set_ylabel('Accuracy ratio')
    if not test_over_len_plot.get_legend():
        test_over_len_plot.legend(loc="upper right")
    plt.pause(0.001)



def plot_run(parsed_logfile_dict):
    """ This function creates a matplotlib for the sequence-to-sequence logfile.
        This plot shows train and test accuracy (over sequence length).

    parsed_logfile_dict: dict which can be obtained from the 'read_logfile' function.
    """
    # setup plot
    plt.ion() # turns plot interactive on, meaning it can update without pausing code
    figure, ((loss_plot, accuracy_plot), (train_over_len_plot, test_over_len_plot)) = plt.subplots(nrows=2, ncols=2)
    plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=0.35)
    figure.set_figwidth(8)
    figure.set_figheight(8)
    loss_plot.set_title("Loss"); loss_plot.set_xlabel('Epochs')
    accuracy_plot.set_title("Accuracy"); accuracy_plot.set_xlabel('Epochs'); accuracy_plot.set_ylim([0, 1.001]); 
    plt.pause(0.001)

    stats = {'epoch_nr':   [info['Epoch'] for info in parsed_logfile_dict],
             'train_loss': [info['Train_loss'] for info in parsed_logfile_dict],
             'train_acc': [info['Train_acc'] for info in parsed_logfile_dict],
             'train_acc_over_len': [info['Train_acc_over_len'] for info in parsed_logfile_dict],
             
             'test_loss': [info['Test_loss'] for info in parsed_logfile_dict],
             'test_acc': [info['Test_acc'] for info in parsed_logfile_dict],
             'test_acc_over_len': [info['Test_acc_over_len'] for info in parsed_logfile_dict],

             'baseline_loss': [info['Base_loss'] for info in parsed_logfile_dict],
             'baseline_acc': [info['Base_acc'] for info in parsed_logfile_dict],
             'base_acc_over_len': [info['Base_acc_over_len'] for info in parsed_logfile_dict],
            }
    plot_graph(statistics=stats, loss_plot=loss_plot, accuracy_plot=accuracy_plot, train_over_len_plot=train_over_len_plot, test_over_len_plot=test_over_len_plot)


def compare_multiple_logsfiles(list_of_logfile_paths, output_csv_path, stat_to_compare="Test_acc"):
    """ This function compares multiple logfiles so one statistic (such as test accuracy)
    can be compared throughout the epochs. This info is written to a .csv file.

    list_of_logfile_paths: a list of paths to logfiles from different training sessions.
    stat_to_compare: Use one of the following options:
                    "Train_loss", "Test_loss", "Base_loss", "Train_acc", 
                    "Test_acc",  "Base_acc"
    output_csv_path: the target path for the new .csv file which will contain the
                    statistic over epochs information of one or more training sessions
    """
    # 0) Check all logfiles exist
    for logfile_path in list_of_logfile_paths:
        if not os_path.exists(logfile_path):
            raise ValueError(f"STOPPED, LOGFILE PATH NOT FOUND: {logfile_path}")

    # 1) Open an empty CSV file to write to
    if os_path.exists(output_csv_path):
        raise ValueError(f"STOPPED, target CSV file '{output_csv_path}' ALREADY EXISTS")
    with open(output_csv_path, 'w', encoding="UTF8", newline='') as output_csv:
        # create CSV writer class
        csv_writer = csv.writer(output_csv)

        # 2) Go over all logfiles given
        logfile_index = -1
        for logfile_path in list_of_logfile_paths:
            logfile_index += 1
            logfile_information = read_logfile(logfile_path)

            # 3) Create header line
            if logfile_index == 0:
                header_list = ['Logfile_index', 'Logfile_path']
                for epoch_dict in logfile_information:
                    epoch = epoch_dict['Epoch']
                    header_list.append(f"Epoch{epoch}_{stat_to_compare}")
                csv_writer.writerow(header_list)
            
            # 4) Add info for this logfile
            info_list = [logfile_index, logfile_path]
            for epoch_dict in logfile_information:
                info_list.append(epoch_dict[stat_to_compare])
            csv_writer.writerow(info_list)


def predict_all_test_sequences(list_of_net_paths, dataset_path, src_vocab, tgt_vocab, output_fasta_path):
    """ This function generates a fasta file for all LSTM-LSTM predictions on the test set.
    This fasta file can be used to create a sequence logo to show amino acid diversity over sequence length
    by uploading this fasta file in http://weblogo.threeplusone.com/create.cgi

    The <unk> <bos> <eos> <pad> tokens are presented as dots (.) in the sequences, as
    this is normally used for unknown amino acids.

    list_of_net_paths: list of saved LSTM-LSTM model paths to include in the sequence (logo) prediction.
    dataset_path: the dataset containing the input for the predicted protein sequences.
    src_vocab: token vocabulary for the promoter. can be obtained using get_promoter_vocab() from Seq2seq_LoadData.py
    tgt_vocab: token vocabulary for the promoter. can be obtained using get_protein_vocab() from Seq2seq_LoadData.py
    output_fasta_path: path to fasta file which stores all predicted protein sequences by the model(s)
    """
    # Define nested function to get a 
    def predicted_tensor_to_string(input_tensor):
        """ Function to get a protein sequence from the predicted tensor in the format
        for a sequence Logo.

        input_tensor: Sequence prediction with shape: [seq_len, number_of_tokens]
        returns: <str> Peptide amino acid sequence to be used for peptide logo
        """
        max_tokens = input_tensor.argmax(dim=1)
        peptide_sequence = ""
        # [1:] to skip the <bos> token always present
        for token in max_tokens[1:]:
            amino_acid = tgt_vocab.idx_to_token[token]
            if amino_acid in ("*", "<bos>", "<eos>", "<pad>"):
                amino_acid = "."
            peptide_sequence = f"{peptide_sequence}{amino_acid}"
        return peptide_sequence


    # Step 0) Check if networks exist
    for net_path in list_of_net_paths:
        if not os_path.exists(net_path):
            raise ValueError(f"PATH DOES NOT EXIST: {net_path}")

    # Step 1) For each network in list
    for net_path in list_of_net_paths:
        # Load model state dicts (.pt) different than full models (.pth)
        if net_path.endswith(".pt"):
            net = get_LSTM_LSTM_model()
            net.load_state_dict(torch.load(net_path))
        else:
            net = torch.load(net_path)
        net.eval()
        train_iter, test_iter = load_data.load_dataset(dataset_path=dataset_path,
                                src_vocab=src_vocab, tgt_vocab=tgt_vocab, batch_size=1)
        with torch.no_grad():
            # For each batch in the test set
            for batch_index, batch in enumerate(test_iter):
                # Predict the batch
                X, X_valid_len, Y, Y_valid_len = [ii for ii in batch]
                Y_pred = net(src=X.permute(1, 0), bos_token=tgt_vocab.token_to_idx['<bos>'],
                            trg=Y.permute(1, 0), teacher_forcing_ratio=0)
                # Write the predicted sequences to the fasta file
                with open(output_fasta_path, 'a') as output_fasta_file:
                    for individual_tensor in Y_pred.permute(1, 0, 2):
                        predicted_seq = predicted_tensor_to_string(individual_tensor)
                        output_fasta_file.write(f">{net_path}___{batch_index}\n")
                        output_fasta_file.write(f"{predicted_seq}\n")
       

def run_main():
    """ Examples of how to use the functions (used in final thesis).
    """
    torch.set_num_threads(2)

    # Create a sequence logo for the Seq2seq forced diversity (with entropy) models
    start_path = r"results\output_and_saved_models\result_PPD_seq2seq_forced_diversity"
    predict_all_test_sequences(list_of_net_paths= \
                            [f"{start_path}/2022_04_26_00h_32m_14s_Epoch99.pth",
                             f"{start_path}/2022_04_26_13h_12m_46s_Epoch99.pth",
                             f"{start_path}/2022_04_26_23h_55m_16s_Epoch99.pth",
                             f"{start_path}/2022_04_27_09h_33m_27s_Epoch99.pth",
                             f"{start_path}/2022_04_27_19h_04m_31s_Epoch99.pth",
                            ],
                         dataset_path=r"ignore_folder\datasets\PPD\2022_03_22_08h_57m_01s__PPD_maxproteinlength_None__MaxPerIdt0.5.txt",
                         src_vocab=load_data.get_promoter_vocab(),
                         tgt_vocab=load_data.get_protein_vocab(add_asterisk_to_vocab=True, use_selenocysteine=True),
                         output_fasta_path='./PPD_forced_diversity_full_ouptut.txt')
    # http://weblogo.threeplusone.com/create.cgi

    # compare the test accuracy between the multiple trained models of the Seq2seq with forced diversity (entropy)
    start_path = r"results\output_and_saved_models\result_PPD_seq2seq_forced_diversity"
    compare_multiple_logsfiles(output_csv_path='./PPD_forced_diversity_testacc.csv',
                               stat_to_compare='Test_acc',
    list_of_logfile_paths=  [f"{start_path}/2022_04_26_00h_32m_14s_seq2seq_log.txt",
                             f"{start_path}/2022_04_26_13h_12m_46s_seq2seq_log.txt",
                             f"{start_path}/2022_04_26_23h_55m_16s_seq2seq_log.txt",
                             f"{start_path}/2022_04_27_09h_33m_27s_seq2seq_log.txt",
                             f"{start_path}/2022_04_27_19h_04m_31s_seq2seq_log.txt",
                             ])

    # DOMAIN CLASSIFIER FINAL DATA
    start_path = r"results\output_and_saved_models\result_PPD_domainclassifier_5runs"
    compare_multiple_logsfiles(output_csv_path='./domain_classifier_ppd_final_TestSens.csv', stat_to_compare="Test_sens",
    list_of_logfile_paths=  [f"{start_path}/2022_05_03_19h_58m_03s_[17, 11, 5]_logfile_DomainClassifier.txt",
                             f"{start_path}/2022_05_03_20h_00m_13s_[17, 11, 5]_logfile_DomainClassifier.txt",
                             f"{start_path}/2022_05_03_20h_02m_27s_[17, 11, 5]_logfile_DomainClassifier.txt",
                             f"{start_path}/2022_05_03_20h_04m_42s_[17, 11, 5]_logfile_DomainClassifier.txt",
                             f"{start_path}/2022_05_03_20h_06m_53s_[17, 11, 5]_logfile_DomainClassifier.txt",
                             ])

    # Plot the accuracy of a logfile
    logfile_test = input("Please give the path to the logfile to parse: ").strip()
    logfile_test = logfile_test.replace('"', '')
    parsed_logfile_output = read_logfile(path_to_logfile=logfile_test)
    plot_run(parsed_logfile_output)
    input() # input so the plot doesnt dissapear


if __name__ == "__main__":
    run_main()

