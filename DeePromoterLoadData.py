""" This file contains the functions related to loading promoter-protein domain data.
This includes functions to encode promoter strings to pytorch tensors, functions to 
read datasets created by 'DeePromoterArtificialData.py' or 'PPD_parser.py', and
a custom Pytorch dataset class to create pytorch data iterators with.
"""
import torch
import random


class PromDomainDataset(torch.utils.data.Dataset):
    """ A custom pytorch dataset class made for promoter-protein domain samples.
    """
    def __init__(self, sample_list, max_prom_len) -> None:
        """ This function initiates the PromDomainDataset class and its values.
        
        max_prom_len: <int> the length of the longest promoter input sequence.
        sample_list: list of all samples as tuples with format 
                    (<int> sample index, <str> 'Train' or 'Test', <str> promoter sequence, <list> domain name strings)
        """
        self.max_prom_len = max_prom_len
        self.domain_token_dict = self.get_domain_token_dict(sample_list=sample_list)
        self.prom_domain_dict = self.create_prom_domain_dict(sample_list=sample_list)


    def __len__(self):
        """
        returns how many samples are in the dataset
        """
        return len(self.prom_domain_dict)
    

    def __getitem__(self, idx):
        """ This function returns a sample based on the index provided.
        This sample contains the promoter input and the desired domain output.
        
        idx: <int> an index in the range of 0 to (dataset size - 1)
        returns: two pytorch tensors:
                    an encoded promoter with shape: [<number of DNA tokens>, <length of promoter>]
                    a domain tensor with shape: [<total number of domains>] (0 if not present, 1 if present)
        """
        return self.prom_domain_dict[idx]
    

    def get_domain_token_dict(self, sample_list):
        """ Create a dictionary with a unique index (number) for all protein domains in the dataset.

        sample_list: list of all samples as tuples with format 
                    (<int> sample index, <str> 'Train' or 'Test', <str> promoter sequence, <list> domain name strings)
        returns:
            <dict> with {<str> domain name: <int> domain token index}
        """
        domain_token_dict = dict()
        for sample_index, sample_type, prom_seq, domain_list in sample_list:
            for domain in domain_list:
                if domain not in domain_token_dict:
                    domain_token_dict[domain] = len(domain_token_dict)

        return domain_token_dict


    def create_prom_domain_dict(self, sample_list):
        """ Create a dictionary to quickly get promoter input and domain output tensors based on an index.

        sample_list: list of all samples as tuples with format 
            (<int> sample index, <str> 'Train' or 'Test', <str> promoter sequence, <list> domain name strings)
        returns:
            <dict> with format:
                {<int> sample_index: (promoter tensor, domain tensor)}
                where the promoter tensor has the shape: [<number of DNA tokens>, <length of promoter>]
                and the domain tensor has the shape: [<total number of domains>] (0 if not present, 1 if present)
        """
        prom_domain_dict = dict()
        # go over all samples
        for sample_index, sample_type, prom_seq, domain_list in sample_list:
            # get encoded promoter
            prom_seq_encoded = encode_promoter(prom_seq, max_length=self.max_prom_len)
            # add tokens for each domain
            domain_token_tensor = torch.zeros(len(self.domain_token_dict))
            for domain in domain_list:
                domain_token = self.domain_token_dict[domain]
                domain_token_tensor[domain_token] = 1
            
            prom_domain_dict[sample_index] = (prom_seq_encoded, domain_token_tensor)

        return prom_domain_dict


def encode_promoter(seq, max_length=None):
    """ Encoder a promoter input sequence into a one hot encoded tensor.

    seq: <str> a promoter input sequence.
    max_length: <int> the length to trim/pad the promoter input to.
    returns: torch float tensor with shape [<number of DNA tokens>, <length of promoter>]
    """
    seq = seq.upper()
    # convert sequence into tokens
    promoter_vocab = {"<pad>":0, "A":1, "C":2, "G":3, "T":4}
    letters = [letter for letter in seq]
    seq = [promoter_vocab[pos] for pos in letters]

    # pad if necesarry
    if max_length:
        if len(seq) < max_length:
            for pad_pos in range(max_length - len(seq)):
                seq.append(promoter_vocab["<pad>"])
        if len(seq) > max_length:
            seq = seq[:max_length]

    """one hot encode the tokens: example ATC would be:
        [1, 0, 0, 0],   A 
        [0, 0, 0, 1],   T
        [0, 1, 0, 0],   C
    """
    seq = torch.tensor(seq)
    seq = torch.nn.functional.one_hot(seq, num_classes=len(promoter_vocab))
    
    return seq.float() # DeePromoter wants floats as input


def parse_dataset(dataset_path):
    """ This function reads a dataset txt file containing promoter sequences
    and domain their associated domains to extract a list of info about each promoter-domain pair.

    dataset_path: path to the tab delimited dataset.
    returns: list of tuples for each sample with the format:
        [(<int> sample index, <str> 'Train' or 'Test', <str> promoter sequence, <list> domain strings, ]
    """
    sample_list = list()
    with open(dataset_path, 'r') as dataset:
        for line in dataset.readlines():
            line = line.strip()
            if line.startswith("#"):
                continue
            sample_index, sample_type, prom_seq, domain_list = line.split("\t")[0:4]
            domain_list = list(domain_list.split(" "))

            sample_index = int(sample_index)

            if sample_type not in ("Train", "Test"):
                raise ValueError(f"Sample with index {sample_index} has an incorrect sample type.")

            sample_list.append((sample_index, sample_type, prom_seq, domain_list))
    
    return sample_list


def load_dataset(dataset_path, batch_size, max_prom_len=None):
    """ This function returns a train pytorch data iter and a test pytorch data iter
    for the specified dataset.

    dataset_path: <str> full path to the dataset with promoter-domain data.
    batch_size: <int> number of samples to use per batch in the data iters.
    max_prom_len: <int> length of the longest promoter input used.
    """
    sample_list = parse_dataset(dataset_path)

    # Get train test indexes
    train_indexes = list()
    test_indexes = list()
    for sample_index, sample_type, prom_seq, domain_list in sample_list:
        if sample_type == "Train":
            train_indexes.append(sample_index)
        elif sample_type == "Test":
            test_indexes.append(sample_index)

    # Check validity of train and indexes
    if len(set(train_indexes).intersection(set(test_indexes))) > 0:
        raise ValueError("Train and test set shares indexes (samples), the dataset is corrupt!")
    if len(train_indexes) < 1 or len(test_indexes) < 1:
        raise ValueError("The train or test set contains no indexes (samples), check dataset used as input!")
    if len(train_indexes) != len(set(train_indexes)) or len(test_indexes) != len(set(test_indexes)):
        raise ValueError("The train or test set has duplicate indexes, the dataset is corrupt!")

    # Create dataset and split into (predetermined) train and test
    dataset = PromDomainDataset(sample_list=sample_list, max_prom_len=max_prom_len)

    train_dataset = torch.utils.data.Subset(dataset=dataset, indices=train_indexes)
    train_iter = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)
    test_dataset = torch.utils.data.Subset(dataset=dataset, indices=test_indexes)
    test_iter = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=batch_size, shuffle=True)

    return train_iter, test_iter


def get_baseline_data_from_data_iter(data_iter, device='cpu'):
    """ Return a data_iter where the promoter-domain pairs are randomly shuffled.
    This was called 'baseline' and can be used to compare model performance compared to random
    (without a promoter-domain relation).

    data_iter: pytorch data iter to shuffle to get a baseline data iter.
    device: device the pytorch tensors are stored on.
    """

    def shuffle_batch(batch):
        """ Encapsulated function to shuffle paired promoter-domain data in a batch.

        batch: tuple with two pytorch tensors.
                promoter tensor with shape: [<number of DNA tokens>, <length of promoter>]
                and domain tensor with shape: [<total number of domains>]
        """
        X, Y= [ii.to(device) for ii in batch]
        batch_size, sequence_length, number_of_nucleotides = X.shape
        
        # get randomized indexes for input
        batch_indexes = [index for index in range(batch_size)]
        random.shuffle(batch_indexes)

        # create empty tensor to store randomized input
        baseline_X = torch.empty(size=X.shape, dtype=X.dtype)

        # loop through shuffled indexes to create the baseline inputs
        for normal_index, shuffled_index in enumerate(batch_indexes):
            baseline_X[normal_index] = X[shuffled_index]
    
        return baseline_X, Y

    # apply the inner shuffle_batch function to each batch
    return [shuffle_batch(batch) for batch in data_iter]


def get_total_domains_in_dataset(path_to_prom_domain_dataset):
    """ Get the total number of unique domains in a promoter-protein domain dataset.

    path_to_prom_domain_dataset: <str>
    returns: <int>
    """
    sample_list = parse_dataset(path_to_prom_domain_dataset)
    unique_domains = set()
    for sample_index, sample_type, prom_seq, domain_list in sample_list:
        for domain in domain_list:
            unique_domains.add(domain)
    
    return len(unique_domains)


if __name__ == "__main__":
    ""